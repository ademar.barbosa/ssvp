package br.com.coretecnologia.resources;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.coretecnologia.SsvpApplication;
import br.com.coretecnologia.domain.Unidade;
import br.com.coretecnologia.dto.CidadeDTO;
import br.com.coretecnologia.dto.CredenciaisDTO;
import br.com.coretecnologia.dto.EnderecoDTO;
import br.com.coretecnologia.dto.ReuniaoDTO;
import br.com.coretecnologia.dto.UfDTO;
import br.com.coretecnologia.dto.UnidadeDTO;
import br.com.coretecnologia.dto.UnidadeEdicaoDTO;
import br.com.coretecnologia.util.TestUtil;

@RunWith(SpringRunner.class)
@TestPropertySource("classpath:ValidationMessages.properties")
@SpringBootTest(classes = SsvpApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UnidadeResourceRoleAdminTests extends TestUtil {

	@LocalServerPort
	private int port;

	private TestRestTemplate restTemplate = new TestRestTemplate();

	private HttpHeaders headers = new HttpHeaders();
	
	private String token;
	
	private String login(CredenciaisDTO creds) {
		HttpEntity<CredenciaisDTO> entity = new HttpEntity<CredenciaisDTO>(creds, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/login", port),
				HttpMethod.POST, entity, Void.class);

		String token = "";
		if (response.getHeaders().get(AUTHORIZATION) != null) {
			token = response.getHeaders().get(AUTHORIZATION).get(0);
		}
		
		return token;
	}
	
	private UnidadeDTO preencherNovaUnidade() {
		UnidadeDTO unidade = new UnidadeDTO();
		unidade.setUnidadeId(null);
		unidade.setUnidadeNome("Conferência de teste");
		unidade.setTelefone("(31) 91234-4657");
		unidade.setEmail("teste@ssvp.com.br");
		unidade.setTipoUnidade("Conferência");
		unidade.setUnidadeSuperior(null);
		
		EnderecoDTO endereco = new EnderecoDTO();
		CidadeDTO cidade = new CidadeDTO(1, "Belo Horizonte");
		
		UfDTO uf = new UfDTO("MG", "Minas Gerais");
		
		endereco.setTipoLogradouro("Rua");
		endereco.setLogradouro("Santana");
		endereco.setNumero("1");
		endereco.setComplemento("Casa");
		endereco.setBairro("Água Branca");
		endereco.setCep("32371080");
		endereco.setCidade(cidade);
		endereco.setUf(uf);
		endereco.setReferenciaEndereco("Próximo à rua cardeal de arco verde");
		unidade.setEndereco(endereco);
		
		ReuniaoDTO reuniao = new ReuniaoDTO();
		reuniao.setDiaSemana("Segunda-feira");
		reuniao.setHorario("20:00");
		unidade.setReuniao(reuniao);
		return unidade;
	}
	
	private ResponseEntity<UnidadeDTO> incluirUnidade() {
		setup();
		UnidadeDTO unidade = preencherNovaUnidade();
		
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(unidade, headers);
		ResponseEntity<UnidadeDTO> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.POST, entity, UnidadeDTO.class);
		return response;
	}
	
	public void excluirUnidade(UnidadeDTO unidade) {
		setup();
		
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(unidade, headers);
		restTemplate.exchange(createURLWithPort("/unidades/" + unidade.getUnidadeId(), port),
				HttpMethod.DELETE, entity, Void.class);
	}
	
	@Before
	public void setup() {
		if (token == null || token.equals("")) {
			CredenciaisDTO creds = new CredenciaisDTO(LOGIN_ADM, SENHA_ADM);
			token = login(creds);
			headers.add(AUTHORIZATION, token);
		}
	}
	
	@Test
	public void testBuscarUnidadePorStatus() throws ClientProtocolException, IOException, JSONException {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		HttpEntity<Unidade> entity = new HttpEntity<Unidade>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/unidades/" + unidadeIncluida.getUnidadeId(), port), HttpMethod.GET,
				entity, String.class);

		excluirUnidade(unidadeIncluida);
		
		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarUnidadesPorStatus() throws ClientProtocolException, IOException, JSONException {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		HttpEntity<Unidade> entity = new HttpEntity<Unidade>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/unidades", port), HttpMethod.GET,
				entity, String.class);
		excluirUnidade(unidadeIncluida);
		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	
	@Test
	public void testIncluirUnidade() {
		UnidadeDTO unidade = preencherNovaUnidade();
		
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(unidade, headers);
		ResponseEntity<UnidadeDTO> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.POST, entity, UnidadeDTO.class);

		excluirUnidade(response.getBody());
		assertTrue(HTTP_STATUS_CREATED.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testExcluirUnidade() {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		
		HttpEntity<Unidade> entity = new HttpEntity<Unidade>(null, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/unidades/"+unidadeIncluida.getUnidadeId(), port), HttpMethod.DELETE,
				entity, Void.class);
		
		assertTrue(HTTP_STATUS_NO_CONTENT.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testAtualizarUnidade() throws JSONException {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		UnidadeEdicaoDTO unidade = new UnidadeEdicaoDTO(unidadeIncluida);
		
		HttpEntity<UnidadeEdicaoDTO> entity = new HttpEntity<UnidadeEdicaoDTO>(unidade, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.PUT, entity, Void.class);

		excluirUnidade(unidadeIncluida);
		assertTrue(HTTP_STATUS_NO_CONTENT.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testGetUnidadesPaginadoTodosParametros() {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		HttpEntity<Void> entity = new HttpEntity<Void>(null, headers);
		ResponseEntity<Void> response = restTemplate.exchange(
				createURLWithPort("/unidades/paginado?pagina=0&linhasPorPagina=10&ordenadoPor=unidadeNome&direcao=ASC", port),
				HttpMethod.GET, entity, Void.class);
		
		excluirUnidade(unidadeIncluida);
		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
}

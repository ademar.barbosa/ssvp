package br.com.coretecnologia.resources;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.coretecnologia.SsvpApplication;
import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.dto.CredenciaisDTO;
import br.com.coretecnologia.dto.MembroDTO;
import br.com.coretecnologia.resources.exceptions.ValidationError;
import br.com.coretecnologia.util.TestUtil;

@RunWith(SpringRunner.class)
@TestPropertySource("classpath:ValidationMessages.properties")
@SpringBootTest(classes = SsvpApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MembroResourceTests extends TestUtil {

	@LocalServerPort
	private int port;

	private TestRestTemplate restTemplate = new TestRestTemplate();

	private HttpHeaders headers = new HttpHeaders();
	
	private String token;
	
	@Autowired
	private BCryptPasswordEncoder pe;
	
	private String login(CredenciaisDTO creds) {
		HttpEntity<CredenciaisDTO> entity = new HttpEntity<CredenciaisDTO>(creds, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/login", port),
				HttpMethod.POST, entity, Void.class);

		String token = "";
		if (response.getHeaders().get(AUTHORIZATION) != null) {
			token = response.getHeaders().get(AUTHORIZATION).get(0);
		}
		
		return token;
	}
	
	public ResponseEntity<MembroDTO> incluirMembro(MembroDTO membroDTO) {
		if (membroDTO == null) {
			membroDTO = getMembroDTO();
		}

		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(membroDTO, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.POST,
				entity, MembroDTO.class);
		
		return response;
	}
	
	public MembroDTO getMembroDTO() {
		MembroDTO membro = new MembroDTO(null, "Teste JUNIT", "testejunit@ssvp.com", "31989947749", "01012000", "Ativo", "Presidente", "Administrador", pe.encode("123"));
		return membro;
	}
	
	public void excluirMembro(MembroDTO membro) {
		setup();
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(membro, headers);
		restTemplate.exchange(createURLWithPort("/membros/" + membro.getMembroId(), port),
				HttpMethod.DELETE, entity, Void.class);
	}
	
	@Before
	public void setup() {
		if (token == null || token.equals("")) {
			CredenciaisDTO creds = new CredenciaisDTO(LOGIN_ADM, SENHA_ADM);
			token = login(creds);
			headers.add(AUTHORIZATION, token);
		}
	}
	
	@Test
	public void testBuscarMembroPorEmail() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<Membro> entity = new HttpEntity<Membro>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/membros/ademar.barbosa@gmail.com/", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarMembroPorEmailSemLogar() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<Membro> entity = new HttpEntity<Membro>(null, null);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/membros/ademar.barbosa@gmail.com/", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarMembroPorEmailNotFound() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<Membro> entity = new HttpEntity<Membro>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/membros/t@teste.com/", port), HttpMethod.GET, 
				entity, String.class);

		assertTrue(HTTP_STATUS_NOT_FOUND.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarTodos() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<Membro> entity = new HttpEntity<Membro>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarTodosSemLogar() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<Membro> entity = new HttpEntity<Membro>(null, null);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarMembroPorMembroId() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroIncluido = incluirMembro(null).getBody();
		
		HttpEntity<Membro> entity = new HttpEntity<Membro>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/membros/" + membroIncluido.getMembroId(), port), HttpMethod.GET,
				entity, String.class);
		
		excluirMembro(membroIncluido);

		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarMembroPorIdSemLogar() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroIncluido = incluirMembro(null).getBody();
		
		HttpEntity<Membro> entity = new HttpEntity<Membro>(null, null);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/membros/" + membroIncluido.getMembroId(), port), HttpMethod.GET,
				entity, String.class);
		
		excluirMembro(membroIncluido);

		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarMembroPorIdNotFound() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<Membro> entity = new HttpEntity<Membro>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/membros/12345778", port), HttpMethod.GET, 
				entity, String.class);

		assertTrue(HTTP_STATUS_NOT_FOUND.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembro() throws ClientProtocolException, IOException, JSONException {
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(null);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_CREATED.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroNomeNulo() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setNome(null);
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroNomeVazio() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setNome("");
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroEmailNulo() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setEmail(null);
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroEmailVazio() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setEmail("");
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroEmailInvalido() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setEmail("testetestete");
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroEmailExistente() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setEmail("ademar.barbosa@gmail.com");
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroTelefoneNulo() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setTelefone(null);
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroTelefoneVazio() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setTelefone("");
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroTelefoneInvalido() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setTelefone("212");
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroDataNascimentoNulo() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setDataNascimento(null);
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroDataNascimentoVazio() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setDataNascimento("");
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroDataNascimentoInvalido() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setDataNascimento("212");
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroDataNascimentoForaIntervalo() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setDataNascimento("01/01/1500");
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroSituacaoMembroNulo() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setSituacaoMembro(null);
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroSituacaoMembroVazio() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setSituacaoMembro("");
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembroSituacaoMembroInvalido() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = getMembroDTO();
		membroDTO.setSituacaoMembro("Teste");
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(membroDTO);
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembro() throws ClientProtocolException, IOException, JSONException {
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(null);
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao.getBody(), headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_NO_CONTENT.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroNomeNulo() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setNome(null);
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroNomeVazio() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setNome("");
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroEmailNulo() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setEmail(null);
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroEmailVazio() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setEmail("");
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroEmailInvalido() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setEmail("teste.dfsfd");;
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroEmailExistente() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setEmail("ademar.barbosa@gmail.com");;
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroTelefoneNulo() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setTelefone(null);
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroTelefoneVazio() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setTelefone("");
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroTelefoneInvalido() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setTelefone("23456");
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroDataNascimentoNulo() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setDataNascimento(null);
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroDataNascimentoVazio() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setDataNascimento("");
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroDataNascimentoInvalido() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setDataNascimento("212");
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroDataNascimentoForaIntervalo() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setDataNascimento("01/01/1500");
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroSituacaoMembroNulo() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setSituacaoMembro(null);
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroSituacaoMembroVazio() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setSituacaoMembro("");
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembroSituacaoMembroInvalido() throws ClientProtocolException, IOException, JSONException {
		MembroDTO respostaInclusao = incluirMembro(null).getBody();
		respostaInclusao.setSituacaoMembro("Teste");
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(respostaInclusao, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembro(respostaInclusao);
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testExcluirMembro() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = incluirMembro(null).getBody();

		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(membroDTO, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/membros/" + membroDTO.getMembroId(), port),
				HttpMethod.DELETE, entity, Void.class);
		
		assertTrue(HTTP_STATUS_NO_CONTENT.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testExcluirMembroNaoExistente() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(null, headers);
		ResponseEntity<ValidationError> response = restTemplate.exchange(createURLWithPort("/membros/" + 56465464, port),
				HttpMethod.DELETE, entity, ValidationError.class);
		
		assertTrue(HTTP_STATUS_NOT_FOUND.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarMembroPorNome() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroIncluido = incluirMembro(null).getBody();
		
		HttpEntity<Membro> entity = new HttpEntity<Membro>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/membros/find/Teste JUNIT/Conselho Particular", port), HttpMethod.GET,
				entity, String.class);
		
		excluirMembro(membroIncluido);

		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarMembroPorNomeTipoUnidadeInvalida() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroIncluido = incluirMembro(null).getBody();
		
		HttpEntity<Membro> entity = new HttpEntity<Membro>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/membros/find/Teste JUNIT/Conselho Teste", port), HttpMethod.GET,
				entity, String.class);
		
		excluirMembro(membroIncluido);

		assertTrue(HTTP_STATUS_BAD_REQUEST.equals(String.valueOf(response.getStatusCode().value())));
	}
	
}

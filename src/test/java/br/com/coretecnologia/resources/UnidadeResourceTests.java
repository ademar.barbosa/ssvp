package br.com.coretecnologia.resources;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.coretecnologia.SsvpApplication;
import br.com.coretecnologia.domain.Unidade;
import br.com.coretecnologia.dto.CidadeDTO;
import br.com.coretecnologia.dto.CredenciaisDTO;
import br.com.coretecnologia.dto.EnderecoDTO;
import br.com.coretecnologia.dto.ReuniaoDTO;
import br.com.coretecnologia.dto.UfDTO;
import br.com.coretecnologia.dto.UnidadeDTO;
import br.com.coretecnologia.dto.UnidadeEdicaoDTO;
import br.com.coretecnologia.dto.UnidadeSuperiorDTO;
import br.com.coretecnologia.resources.exceptions.ValidationError;
import br.com.coretecnologia.util.TestUtil;

@RunWith(SpringRunner.class)
@TestPropertySource("classpath:ValidationMessages.properties")
@SpringBootTest(classes = SsvpApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UnidadeResourceTests extends TestUtil {

	@LocalServerPort
	private int port;

	private TestRestTemplate restTemplate = new TestRestTemplate();

	private HttpHeaders headers = new HttpHeaders();

	@Value("${validation.field.contato.sempresidente.message}")
	private String MSG_VALIDACAO_UNIDADE_SEM_PRESIDENTE;

	@Value("${validation.field.unidadevinculada.naoencontrada.message}")
	private String MSG_VALIDACAO_UNIDADE_VINCULADA_NAO_ENCONTRADA;
	
	@Value("${validation.field.unidadevinculada.erro.hierarquia.message}")
	private String MSG_VALIDACAO_UNIDADE_HIERARQUIA_INVALIDA;

	@Value("${validation.field.notEnpty.message}")
	private String MSG_VALIDACAO_UNIDADE_LISTA_VAZIA;
	
	private String token;
	
	private String login(CredenciaisDTO creds) {
		HttpEntity<CredenciaisDTO> entity = new HttpEntity<CredenciaisDTO>(creds, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/login", port),
				HttpMethod.POST, entity, Void.class);

		String token = "";
		if (response.getHeaders().get(AUTHORIZATION) != null) {
			token = response.getHeaders().get(AUTHORIZATION).get(0);
		}
		
		return token;
	}
	
	private UnidadeDTO preencherNovaUnidade() {
		UnidadeDTO unidade = new UnidadeDTO();
		unidade.setUnidadeId(null);
		unidade.setUnidadeNome("Conferência de teste");
		unidade.setTelefone("(31) 91234-4657");
		unidade.setEmail("teste@ssvp.com.br");
		unidade.setTipoUnidade("Conferência");
		unidade.setUnidadeSuperior(null);
		
		EnderecoDTO endereco = new EnderecoDTO();
		CidadeDTO cidade = new CidadeDTO(1, "Belo Horizonte");
		
		UfDTO uf = new UfDTO("MG", "Minas Gerais");
		
		endereco.setTipoLogradouro("Rua");
		endereco.setLogradouro("Santana");
		endereco.setNumero("1");
		endereco.setComplemento("Casa");
		endereco.setBairro("Água Branca");
		endereco.setCep("32371080");
		endereco.setCidade(cidade);
		endereco.setUf(uf);
		endereco.setReferenciaEndereco("Próximo à rua cardeal de arco verde");
		unidade.setEndereco(endereco);
		
		ReuniaoDTO reuniao = new ReuniaoDTO();
		reuniao.setDiaSemana("Segunda-feira");
		reuniao.setHorario("20:00");
		unidade.setReuniao(reuniao);
		return unidade;
	}
	
	private UnidadeDTO preencherNovaUnidadeConselhoParticular() {
		UnidadeDTO unidade = new UnidadeDTO();
		unidade.setUnidadeId(null);
		unidade.setUnidadeNome("Conselho Particular de teste");
		unidade.setTelefone("(31) 91234-4657");
		unidade.setEmail("teste@ssvp.com.br");
		unidade.setTipoUnidade("Conselho Particular");
		unidade.setUnidadeSuperior(null);
		
		EnderecoDTO endereco = new EnderecoDTO();
		CidadeDTO cidade = new CidadeDTO(1, "Belo Horizonte");
		
		UfDTO uf = new UfDTO("MG", "Minas Gerais");
		
		endereco.setTipoLogradouro("Rua");
		endereco.setLogradouro("Santana");
		endereco.setNumero("1");
		endereco.setComplemento("Casa");
		endereco.setBairro("Água Branca");
		endereco.setCep("32371080");
		endereco.setCidade(cidade);
		endereco.setUf(uf);
		endereco.setReferenciaEndereco("Próximo à rua cardeal de arco verde");
		unidade.setEndereco(endereco);
		
		ReuniaoDTO reuniao = new ReuniaoDTO();
		reuniao.setDiaSemana("Segunda-feira");
		reuniao.setHorario("20:00");
		unidade.setReuniao(reuniao);
		return unidade;
	}
	
	private UnidadeDTO preencherNovaUnidadeComVinculo(UnidadeSuperiorDTO unidadeSuperior) {
		UnidadeDTO unidade = new UnidadeDTO();
		unidade.setUnidadeId(null);
		unidade.setUnidadeNome("Conferência de teste 2");
		unidade.setTelefone("(31) 91234-4657");
		unidade.setEmail("teste@ssvp.com.br");
		unidade.setTipoUnidade("Conferência");
		unidade.setUnidadeSuperior(null);
		
		EnderecoDTO endereco = new EnderecoDTO();
		CidadeDTO cidade = new CidadeDTO(1, "Belo Horizonte");
		
		UfDTO uf = new UfDTO("MG", "Minas Gerais");
		
		endereco.setTipoLogradouro("Rua");
		endereco.setLogradouro("Santana");
		endereco.setNumero("1");
		endereco.setComplemento("Casa");
		endereco.setBairro("Água Branca");
		endereco.setCep("32371080");
		endereco.setCidade(cidade);
		endereco.setUf(uf);
		endereco.setReferenciaEndereco("Próximo à rua cardeal de arco verde");
		unidade.setEndereco(endereco);
		
		ReuniaoDTO reuniao = new ReuniaoDTO();
		reuniao.setDiaSemana("Segunda-feira");
		reuniao.setHorario("20:00");
		unidade.setReuniao(reuniao);
		
		unidade.setUnidadeSuperior(unidadeSuperior);
		
		return unidade;
	}
	
	private ResponseEntity<UnidadeDTO> incluirUnidade() {
		setup();
		UnidadeDTO unidade = preencherNovaUnidade();
		
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(unidade, headers);
		ResponseEntity<UnidadeDTO> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.POST, entity, UnidadeDTO.class);
		return response;
	}
	
	private ResponseEntity<UnidadeDTO> incluirUnidadeConselhoParticular(UnidadeDTO unidadeSuperior) {
		setup();
		UnidadeDTO unidade = preencherNovaUnidadeConselhoParticular();
		
		if (unidadeSuperior != null) {
			unidade.setUnidadeSuperior(new UnidadeSuperiorDTO(null, unidadeSuperior.getUnidadeId(), unidadeSuperior.getUnidadeNome()));
		}
		
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(unidade, headers);
		ResponseEntity<UnidadeDTO> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.POST, entity, UnidadeDTO.class);
		return response;
	}
	
	private ResponseEntity<UnidadeDTO> incluirUnidadeComSuperior(UnidadeDTO unidadeSuperior) {
		setup();
		UnidadeDTO unidadeDependente = preencherNovaUnidadeComVinculo(new UnidadeSuperiorDTO());
		unidadeDependente.setUnidadeSuperior(new UnidadeSuperiorDTO(null, unidadeSuperior.getUnidadeId(), unidadeSuperior.getUnidadeNome()));
		
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(unidadeDependente, headers);
		ResponseEntity<UnidadeDTO> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.POST, entity, UnidadeDTO.class);
		return response;
	}
	
	public void excluirUnidade(UnidadeDTO unidade) {
		setup();
		
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(unidade, headers);
		restTemplate.exchange(createURLWithPort("/unidades/" + unidade.getUnidadeId(), port),
				HttpMethod.DELETE, entity, Void.class);
	}
	
	@Before
	public void setup() {
		if (token == null || token.equals("")) {
			CredenciaisDTO creds = new CredenciaisDTO(LOGIN_ADM, SENHA_ADM);
			token = login(creds);
			headers.add(AUTHORIZATION, token);
		}
	}
	
	@Test
	public void testBuscarUnidade() throws ClientProtocolException, IOException, JSONException {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/unidades/" + unidadeIncluida.getUnidadeId(), port), HttpMethod.GET,
				entity, String.class);

		String expected = "{\"unidadeNome\":\"Conferência de teste\"}";

		excluirUnidade(unidadeIncluida);
		
		JSONAssert.assertEquals(expected, response.getBody(), JSONCompareMode.LENIENT);
	}

	@Test
	public void testBuscarUnidadePorStatus() throws ClientProtocolException, IOException, JSONException {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		HttpEntity<Unidade> entity = new HttpEntity<Unidade>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/unidades/" + unidadeIncluida.getUnidadeId(), port), HttpMethod.GET,
				entity, String.class);

		excluirUnidade(unidadeIncluida);
		
		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarUnidadeSemLogar() throws ClientProtocolException, IOException, JSONException {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		HttpEntity<Unidade> entity = new HttpEntity<Unidade>(null, null);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/unidades/" + unidadeIncluida.getUnidadeId(), port), HttpMethod.GET,
				entity, String.class);

		excluirUnidade(unidadeIncluida);
		
		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}

	@Test
	public void testUnidadeNotFound() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<Unidade> entity = new HttpEntity<Unidade>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/unidades/1234546", port),
				HttpMethod.GET, entity, String.class);

		assertTrue(HTTP_STATUS_NOT_FOUND.equals(String.valueOf(response.getStatusCode().value())));
	}

	@Test
	public void testBuscarUnidades() throws ClientProtocolException, IOException, JSONException {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(null, headers);
		ResponseEntity<List<UnidadeDTO>> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.GET, entity, new ParameterizedTypeReference<List<UnidadeDTO>>() {
				});
		List<UnidadeDTO> lsUnidade = response.getBody();
		excluirUnidade(unidadeIncluida);
		assertTrue(lsUnidade.size() > 0);
	}
	
	@Test
	public void testBuscarUnidadesSuperiores() throws ClientProtocolException, IOException, JSONException {
		UnidadeDTO unidadeIncluida = incluirUnidadeConselhoParticular(null).getBody();
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(null, headers);
		ResponseEntity<List<UnidadeDTO>> response = restTemplate.exchange(createURLWithPort("/unidades?tipoUnidade=Conselho Particular", port),
				HttpMethod.GET, entity, new ParameterizedTypeReference<List<UnidadeDTO>>() {
				});
		List<UnidadeDTO> lsUnidade = response.getBody();
		excluirUnidade(unidadeIncluida);
		assertTrue(lsUnidade.size() > 0);
	}

	@Test
	public void testBuscarUnidadesPorStatus() throws ClientProtocolException, IOException, JSONException {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		HttpEntity<Unidade> entity = new HttpEntity<Unidade>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/unidades", port), HttpMethod.GET,
				entity, String.class);
		excluirUnidade(unidadeIncluida);
		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarUnidadesSemLogar() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<Unidade> entity = new HttpEntity<Unidade>(null, null);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/unidades", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	
	@Test
	public void testIncluirUnidade() {
		ResponseEntity<UnidadeDTO> response = incluirUnidade();
		
		excluirUnidade(response.getBody());

		assertTrue(HTTP_STATUS_CREATED.equals(String.valueOf(response.getStatusCode().value())));
	}

	@Test
	public void testIncluirUnidadeComUnidadeSuperior() {
		UnidadeDTO unidade = incluirUnidadeConselhoParticular(null).getBody();
		ResponseEntity<UnidadeDTO> unidadeDependente = incluirUnidadeComSuperior(unidade);
		
		excluirUnidade(unidadeDependente.getBody());
		excluirUnidade(unidade);
		
		assertTrue(HTTP_STATUS_CREATED.equals(String.valueOf(unidadeDependente.getStatusCode().value())));
	}

	@Test
	public void testIncluirSemLogar() {
		UnidadeDTO unidade = preencherNovaUnidade();
		
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(unidade, null);
		ResponseEntity<ValidationError> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.POST, entity, ValidationError.class);

		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}

	@Test
	public void testIncluirUnidadeWithErrors() throws JSONException {
		UnidadeDTO unidade = preencherNovaUnidade();
		unidade.setUnidadeNome(null);
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(unidade, headers);
		ResponseEntity<ValidationError> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.POST, entity, ValidationError.class);

		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirUnidadeIdExistente() throws JSONException {
		UnidadeDTO unidade = incluirUnidade().getBody();
		
		UnidadeDTO novaUnidade = preencherNovaUnidade();
		novaUnidade.setUnidadeId(unidade.getUnidadeId());
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(unidade, headers);
		ResponseEntity<ValidationError> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.POST, entity, ValidationError.class);

		excluirUnidade(unidade);
		
		String campoValidar = "unidadeId";
		Long qtdadeErro = response.getBody().getErrors().stream().filter(obj -> obj.getFieldName().equals(campoValidar))
				.collect(Collectors.counting());
		assertTrue(qtdadeErro == 1);
	}

	@Test
	public void testIncluirUnidadeNomeExistente() throws JSONException {
		UnidadeDTO unidade = incluirUnidade().getBody();
		UnidadeDTO novaUnidade = preencherNovaUnidade();
		novaUnidade.setUnidadeNome(unidade.getUnidadeNome());
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(novaUnidade, headers);
		ResponseEntity<ValidationError> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.POST, entity, ValidationError.class);

		excluirUnidade(unidade);
		
		String campoValidar = "unidadeNome";
		Long qtdadeErro = response.getBody().getErrors().stream().filter(obj -> obj.getFieldName().equals(campoValidar))
				.collect(Collectors.counting());
		assertTrue(qtdadeErro == 1);
	}

	@Test
	public void testIncluirUnidadeComUnidadeSuperiorInvalida() throws JSONException {
		UnidadeDTO unidade = incluirUnidade().getBody();
		UnidadeDTO unidadeDependente = preencherNovaUnidade();
		unidadeDependente.setUnidadeSuperior(new UnidadeSuperiorDTO(null, 234234, ""));
		
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(unidadeDependente, headers);
		ResponseEntity<ValidationError> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.POST, entity, ValidationError.class);

		excluirUnidade(unidadeDependente);
		excluirUnidade(unidade);
		
		String campoValidar = "unidadeSuperior";
		Long qtdadeErro = response.getBody().getErrors().stream()
				.filter(obj -> obj.getFieldName().equals(campoValidar)
						&& obj.getMessage().equals(MSG_VALIDACAO_UNIDADE_VINCULADA_NAO_ENCONTRADA))
				.collect(Collectors.counting());
		assertTrue(qtdadeErro >= 1);
	}
	
	@Test
	public void testIncluirUnidadeComErroHierarquia() throws JSONException {
		UnidadeDTO unidade = incluirUnidade().getBody();
		UnidadeDTO unidadeDependente = preencherNovaUnidade();
		unidadeDependente.setUnidadeSuperior(new UnidadeSuperiorDTO(null, unidade.getUnidadeId(), unidade.getUnidadeNome()));
		
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(unidadeDependente, headers);
		ResponseEntity<ValidationError> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.POST, entity, ValidationError.class);

		excluirUnidade(unidadeDependente);
		excluirUnidade(unidade);
		
		String campoValidar = "unidadeSuperior";
		Long qtdadeErro = response.getBody().getErrors().stream()
				.filter(obj -> 
				obj.getFieldName().equals(campoValidar) && 
				obj.getMessage().equals(MSG_VALIDACAO_UNIDADE_HIERARQUIA_INVALIDA))
				.collect(Collectors.counting());
		assertTrue(qtdadeErro >= 1);
	}

	@Test
	public void testExcluirComUnidadeVinculada() {
		UnidadeDTO unidadeDependente = incluirUnidadeConselhoParticular(null).getBody();
		ResponseEntity<UnidadeDTO> unidade = incluirUnidadeComSuperior(unidadeDependente);
		
		excluirUnidade(unidade.getBody());
		excluirUnidade(unidadeDependente);
		
		assertTrue(HTTP_STATUS_CREATED.equals(String.valueOf(unidade.getStatusCode().value())));
	}
	
	@Test
	public void testExcluirUnidade() {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		
		HttpEntity<Unidade> entity = new HttpEntity<Unidade>(null, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/unidades/"+unidadeIncluida.getUnidadeId(), port), HttpMethod.DELETE,
				entity, Void.class);
		
		assertTrue(HTTP_STATUS_NO_CONTENT.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testAtualizarUnidadeWithErrors() throws JSONException {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		UnidadeEdicaoDTO unidade = new UnidadeEdicaoDTO(unidadeIncluida);
		unidade.setUnidadeNome(null);
		HttpEntity<UnidadeEdicaoDTO> entity = new HttpEntity<UnidadeEdicaoDTO>(unidade, headers);
		ResponseEntity<ValidationError> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.PUT, entity, ValidationError.class);

		excluirUnidade(unidadeIncluida);
		
		assertTrue(HTTP_STATUS_UNPROCESSABLE_ENTITY.equals(String.valueOf(response.getStatusCode().value())));
	}

	@Test
	public void testAtualizarUnidadeNomeExistente() throws JSONException {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		UnidadeEdicaoDTO unidade = new UnidadeEdicaoDTO(unidadeIncluida);
		unidade.setUnidadeNome("Conferência de teste");
		unidade.setUnidadeId(new Integer("2000000"));
		
		HttpEntity<UnidadeEdicaoDTO> entity = new HttpEntity<UnidadeEdicaoDTO>(unidade, headers);
		ResponseEntity<ValidationError> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.POST, entity, ValidationError.class);

		excluirUnidade(unidadeIncluida);
		
		String campoValidar = "unidadeNome";
		Long qtdadeErro = response.getBody().getErrors().stream().filter(obj -> obj.getFieldName().equals(campoValidar))
				.collect(Collectors.counting());
		assertTrue(qtdadeErro == 1);
	}

	@Test
	public void testAtualizarUnidadeComUnidadeVinculadaInvalida() throws JSONException {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		UnidadeEdicaoDTO unidade = new UnidadeEdicaoDTO(unidadeIncluida);
		
		unidade.setUnidadeSuperior(new UnidadeSuperiorDTO(null, 548748476, ""));
		
		HttpEntity<UnidadeEdicaoDTO> entity = new HttpEntity<UnidadeEdicaoDTO>(unidade, headers);
		ResponseEntity<ValidationError> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.PUT, entity, ValidationError.class);

		excluirUnidade(unidadeIncluida);
		
		String campoValidar = "unidadeSuperior";
		Long qtdadeErro = response.getBody().getErrors().stream()
				.filter(obj -> obj.getFieldName().equals(campoValidar)
						&& obj.getMessage().equals(MSG_VALIDACAO_UNIDADE_VINCULADA_NAO_ENCONTRADA))
				.collect(Collectors.counting());
		assertTrue(qtdadeErro == 1);
	}
	
	@Test
	public void testAtualizarUnidadeComErroHierarquia() throws JSONException {
		UnidadeDTO unidade = incluirUnidade().getBody();
		UnidadeDTO unidadeDependente = incluirUnidadeConselhoParticular(null).getBody();
		unidadeDependente.setUnidadeSuperior(new UnidadeSuperiorDTO(null, unidade.getUnidadeId(), unidade.getUnidadeNome()));
		
		HttpEntity<UnidadeDTO> entity = new HttpEntity<UnidadeDTO>(unidadeDependente, headers);
		ResponseEntity<ValidationError> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.PUT, entity, ValidationError.class);

		excluirUnidade(unidadeDependente);
		excluirUnidade(unidade);
		
		String campoValidar = "unidadeSuperior";
		Long qtdadeErro = response.getBody().getErrors().stream()
				.filter(obj -> 
				obj.getFieldName().equals(campoValidar) && 
				obj.getMessage().equals(MSG_VALIDACAO_UNIDADE_HIERARQUIA_INVALIDA))
				.collect(Collectors.counting());
		assertTrue(qtdadeErro >= 1);
	}

	@Test
	public void testAtualizarUnidadeNaoCadastrada() {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		UnidadeEdicaoDTO unidade = new UnidadeEdicaoDTO(unidadeIncluida);
		unidade.setUnidadeId(548748476);
		unidade.setUnidadeNome("Teste inclusão 2020");
		HttpEntity<UnidadeEdicaoDTO> entity = new HttpEntity<UnidadeEdicaoDTO>(unidade, headers);
		ResponseEntity<ValidationError> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.PUT, entity, ValidationError.class);

		excluirUnidade(unidadeIncluida);
		assertTrue(HTTP_STATUS_BAD_REQUEST.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testAtualizarUnidade() throws JSONException {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		UnidadeEdicaoDTO unidade = new UnidadeEdicaoDTO(unidadeIncluida);
		
		HttpEntity<UnidadeEdicaoDTO> entity = new HttpEntity<UnidadeEdicaoDTO>(unidade, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/unidades", port),
				HttpMethod.PUT, entity, Void.class);

		excluirUnidade(unidadeIncluida);
		assertTrue(HTTP_STATUS_NO_CONTENT.equals(String.valueOf(response.getStatusCode().value())));
	}

	@Test
	public void testGetUnidadesPaginadoTodosParametros() {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		HttpEntity<Void> entity = new HttpEntity<Void>(null, headers);
		ResponseEntity<Void> response = restTemplate.exchange(
				createURLWithPort("/unidades/paginado?pagina=0&linhasPorPagina=10&ordenadoPor=unidadeNome&direcao=ASC", port),
				HttpMethod.GET, entity, Void.class);
		
		excluirUnidade(unidadeIncluida);
		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testGetUnidadesPaginadoSemDirecao() {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		HttpEntity<Void> entity = new HttpEntity<Void>(null, headers);
		ResponseEntity<Void> response = restTemplate.exchange(
				createURLWithPort("/unidades/paginado?pagina=0&linhasPorPagina=10&ordenadoPor=unidadeNome", port),
				HttpMethod.GET, entity, Void.class);
		
		excluirUnidade(unidadeIncluida);
		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testGetUnidadesPaginadoSemOrdenadoEDirecao() {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		HttpEntity<Void> entity = new HttpEntity<Void>(null, headers);
		ResponseEntity<Void> response = restTemplate.exchange(
				createURLWithPort("/unidades/paginado?pagina=0&linhasPorPagina=10", port),
				HttpMethod.GET, entity, Void.class);
		
		excluirUnidade(unidadeIncluida);
		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testGetUnidadesPaginadoSemLinhasEOrdenadoEDirecao() {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		HttpEntity<Void> entity = new HttpEntity<Void>(null, headers);
		ResponseEntity<Void> response = restTemplate.exchange(
				createURLWithPort("/unidades/paginado?pagina=0", port),
				HttpMethod.GET, entity, Void.class);
		
		excluirUnidade(unidadeIncluida);
		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testGetUnidadesPaginadoSemPaginaELinhasEOrdenadoEDirecao() {
		UnidadeDTO unidadeIncluida = incluirUnidade().getBody();
		HttpEntity<Void> entity = new HttpEntity<Void>(null, headers);
		ResponseEntity<Void> response = restTemplate.exchange(
				createURLWithPort("/unidades/paginado", port),
				HttpMethod.GET, entity, Void.class);
		
		excluirUnidade(unidadeIncluida);
		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	/*@Test
	public void testGetMembrosUnidade() {
		/unidades/{unidadeId}/membros
	}
	
	@Test
	public void testGetMembrosUnidadeVazio() {
		/unidades/{unidadeId}/membros
	}
	
	@Test
	public void testAssociarMembro() {
		/unidades/associarMembro
	}
	
	@Test
	public void testAssociarMembroInexistente() {
		/unidades/associarMembro
	}
	
	@Test
	public void testAssociarMembroUnidadeInexistente() {
		/unidades/associarMembro
	}
	
	@Test
	public void testAssociarMembroJaAssociadoOutraUnidade() {
		/unidades/associarMembro
	}
	
	@Test
	public void testDesassociarMembros() {
		/unidades/desassociarMembro/{unidadeId}/{membroId}
	}*/
	
}

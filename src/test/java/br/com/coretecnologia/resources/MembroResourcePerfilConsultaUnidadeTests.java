package br.com.coretecnologia.resources;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.coretecnologia.SsvpApplication;
import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.dto.CredenciaisDTO;
import br.com.coretecnologia.dto.MembroDTO;
import br.com.coretecnologia.util.TestUtil;

@RunWith(SpringRunner.class)
@TestPropertySource("classpath:ValidationMessages.properties")
@SpringBootTest(classes = SsvpApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MembroResourcePerfilConsultaUnidadeTests extends TestUtil {

	@LocalServerPort
	private int port;

	private TestRestTemplate restTemplate = new TestRestTemplate();

	private HttpHeaders headers = new HttpHeaders();
	private HttpHeaders headersAdm = new HttpHeaders();
	
	private String token;
	
	@Autowired
	private BCryptPasswordEncoder pe;
	
	private String login(CredenciaisDTO creds) {
		HttpEntity<CredenciaisDTO> entity = new HttpEntity<CredenciaisDTO>(creds, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/login", port),
				HttpMethod.POST, entity, Void.class);

		String token = "";
		if (response.getHeaders().get(AUTHORIZATION) != null) {
			token = response.getHeaders().get(AUTHORIZATION).get(0);
		}
		
		return token;
	}
	
	public ResponseEntity<MembroDTO> incluirMembro(MembroDTO membroDTO) {
		if (membroDTO == null) {
			membroDTO = getMembroDTO();
		}

		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(membroDTO, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.POST,
				entity, MembroDTO.class);
		
		return response;
	}
	
	public ResponseEntity<MembroDTO> incluirMembroComoAdm(MembroDTO membroDTO) {
		if (membroDTO == null) {
			membroDTO = getMembroDTO();
		}
		
		CredenciaisDTO creds = new CredenciaisDTO(LOGIN_ADM, SENHA_ADM);
		headersAdm.add(AUTHORIZATION, login(creds));

		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(membroDTO, headersAdm);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.POST,
				entity, MembroDTO.class);
		
		return response;
	}
	
	public MembroDTO getMembroDTO() {
		MembroDTO membro = new MembroDTO(null, "Teste JUNIT", "testejunit@ssvp.com", "31989947749", "01012000", "Ativo", "Presidente", "Administrador", pe.encode("123"));
		return membro;
	}
	
	public ResponseEntity<Void> excluirMembro(MembroDTO membro) {
		setup();
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(membro, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/membros/" + membro.getMembroId(), port),
				HttpMethod.DELETE, entity, Void.class);
		
		return response;
	}
	
	public void excluirMembroComoAdm(MembroDTO membro) {
		CredenciaisDTO creds = new CredenciaisDTO(LOGIN_ADM, SENHA_ADM);
		headersAdm.add(AUTHORIZATION, login(creds));
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(membro, headersAdm);
		restTemplate.exchange(createURLWithPort("/membros/" + membro.getMembroId(), port),
				HttpMethod.DELETE, entity, Void.class);
	}
	
	@Before
	public void setup() {
		if (token == null || token.equals("")) {
			CredenciaisDTO creds = new CredenciaisDTO(LOGIN_CONSULTA_UNIDADE, SENHA_CONSULTA_UNIDADE);
			token = login(creds);
			headers.add(AUTHORIZATION, token);
		}
	}
	
	@Test
	public void testBuscarMembroPorEmail() throws ClientProtocolException, IOException, JSONException {
		ResponseEntity<MembroDTO> responseIncluir = incluirMembroComoAdm(null);
		MembroDTO membroIncluido = responseIncluir.getBody();
		
		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(membroIncluido, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/membros/" + membroIncluido.getEmail() + "/", port), HttpMethod.GET,
				entity, String.class);

		excluirMembroComoAdm(membroIncluido);
		
		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarTodos() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<Membro> entity = new HttpEntity<Membro>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarMembroPorMembroId() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroIncluido = incluirMembroComoAdm(null).getBody();
		
		HttpEntity<Membro> entity = new HttpEntity<Membro>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/membros/" + membroIncluido.getMembroId(), port), HttpMethod.GET,
				entity, String.class);
		
		excluirMembroComoAdm(membroIncluido);

		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testIncluirMembro() throws ClientProtocolException, IOException, JSONException {
		ResponseEntity<MembroDTO> respostaInclusao = incluirMembro(null);
		excluirMembroComoAdm(respostaInclusao.getBody());
		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(respostaInclusao.getStatusCode().value())));
	}
	
	@Test
	public void testEditarMembro() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = incluirMembroComoAdm(null).getBody();

		HttpEntity<MembroDTO> entity = new HttpEntity<MembroDTO>(membroDTO, headers);
		ResponseEntity<MembroDTO> response = restTemplate.exchange(createURLWithPort("/membros", port), HttpMethod.PUT,
				entity, MembroDTO.class);
		
		excluirMembroComoAdm(membroDTO);
		
		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testExcluirMembro() throws ClientProtocolException, IOException, JSONException {
		MembroDTO membroDTO = incluirMembroComoAdm(null).getBody();

		ResponseEntity<Void> response = excluirMembro(membroDTO);
		
		excluirMembroComoAdm(membroDTO);
		
		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}
	
}

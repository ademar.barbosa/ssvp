package br.com.coretecnologia.resources;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import br.com.coretecnologia.SsvpApplication;
import br.com.coretecnologia.dto.CredenciaisDTO;
import br.com.coretecnologia.dto.EmailDTO;
import br.com.coretecnologia.util.TestUtil;

@RunWith(SpringRunner.class)
@TestPropertySource("classpath:ValidationMessages.properties")
@SpringBootTest(classes = SsvpApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthResourceTests extends TestUtil {

	@LocalServerPort
	private int port;
	
	private TestRestTemplate restTemplate = new TestRestTemplate();
	
	private HttpHeaders headers = new HttpHeaders();
	
	private String login(CredenciaisDTO creds) {
		HttpEntity<CredenciaisDTO> entity = new HttpEntity<CredenciaisDTO>(creds, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/login", port),
				HttpMethod.POST, entity, Void.class);

		String token = "";
		if (response.getHeaders().get(AUTHORIZATION) != null) {
			token = response.getHeaders().get(AUTHORIZATION).get(0);
		}
		
		return token;
	}
	
	@Test
	public void testLogin() {
		CredenciaisDTO creds = new CredenciaisDTO(LOGIN_ADM, SENHA_ADM);
		String token = login(creds);
		assertTrue(!token.equals(""));
	}
	
	@Test
	public void testLoginSenhaIncorreta() {
		CredenciaisDTO creds = new CredenciaisDTO(LOGIN_ADM, SENHA_ADM + "1");
		String token = login(creds);
		assertTrue(token.equals(""));
	}

	@Test
	public void testRefreshToken() {
		CredenciaisDTO creds = new CredenciaisDTO(LOGIN_ADM, SENHA_ADM);
		String token = login(creds);
		EmailDTO emailDTO = new EmailDTO(LOGIN_ADM);
		
		headers.add(AUTHORIZATION, token);
		HttpEntity<EmailDTO> entity = new HttpEntity<EmailDTO>(emailDTO, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/auth/refresh_token", port),
				HttpMethod.POST, entity, Void.class);
		
		if (response.getHeaders().get(AUTHORIZATION) != null) {
			token = response.getHeaders().get(AUTHORIZATION).get(0);
		}
		
		assertTrue(!token.equals(""));
		assertTrue(HTTP_STATUS_NO_CONTENT.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testRefreshTokenInvalido() {
		CredenciaisDTO creds = new CredenciaisDTO(LOGIN_ADM, SENHA_ADM);
		String token = login(creds);
		EmailDTO emailDTO = new EmailDTO(LOGIN_ADM);
		
		headers.add(AUTHORIZATION, token + "1");
		HttpEntity<EmailDTO> entity = new HttpEntity<EmailDTO>(emailDTO, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/auth/refresh_token", port),
				HttpMethod.POST, entity, Void.class);
		
		token = "";
		if (response.getHeaders().get(AUTHORIZATION) != null) {
			token = response.getHeaders().get(AUTHORIZATION).get(0);
		}
		
		assertTrue(token.equals(""));
		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testRefreshTokenSemLogar() {
		EmailDTO emailDTO = new EmailDTO(LOGIN_ADM);
		
		HttpEntity<EmailDTO> entity = new HttpEntity<EmailDTO>(emailDTO, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/auth/refresh_token", port),
				HttpMethod.POST, entity, Void.class);
		
		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testHeartbeat() {
		CredenciaisDTO creds = new CredenciaisDTO(LOGIN_ADM, SENHA_ADM);
		String token = login(creds);
		EmailDTO emailDTO = new EmailDTO(LOGIN_ADM);
		
		headers.add(AUTHORIZATION, token);
		HttpEntity<EmailDTO> entity = new HttpEntity<EmailDTO>(emailDTO, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/auth/heartbeat", port),
				HttpMethod.POST, entity, Void.class);
		
		if (response.getHeaders().get(AUTHORIZATION) != null) {
			token = response.getHeaders().get(AUTHORIZATION).get(0);
		}
		
		assertTrue(!token.equals(""));
		assertTrue(HTTP_STATUS_NO_CONTENT.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testHeartbeatTokenInvalido() {
		CredenciaisDTO creds = new CredenciaisDTO(LOGIN_ADM, SENHA_ADM);
		String token = login(creds);
		EmailDTO emailDTO = new EmailDTO(LOGIN_ADM);
		
		headers.add(AUTHORIZATION, token + "1");
		HttpEntity<EmailDTO> entity = new HttpEntity<EmailDTO>(emailDTO, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/auth/heartbeat", port),
				HttpMethod.POST, entity, Void.class);
		
		token = "";
		if (response.getHeaders().get(AUTHORIZATION) != null) {
			token = response.getHeaders().get(AUTHORIZATION).get(0);
		}
		
		assertTrue(token.equals(""));
		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testHeartbeatSemLogar() {
		EmailDTO emailDTO = new EmailDTO(LOGIN_ADM);
		
		HttpEntity<EmailDTO> entity = new HttpEntity<EmailDTO>(emailDTO, headers);
		ResponseEntity<Void> response = restTemplate.exchange(createURLWithPort("/auth/heartbeat", port),
				HttpMethod.POST, entity, Void.class);
		
		assertTrue(HTTP_STATUS_FORBIDDEN.equals(String.valueOf(response.getStatusCode().value())));
	}
	
}

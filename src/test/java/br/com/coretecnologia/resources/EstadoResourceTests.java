package br.com.coretecnologia.resources;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.coretecnologia.SsvpApplication;
import br.com.coretecnologia.dto.UfDTO;
import br.com.coretecnologia.util.TestUtil;

@RunWith(SpringRunner.class)
@TestPropertySource("classpath:ValidationMessages.properties")
@SpringBootTest(classes = SsvpApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EstadoResourceTests extends TestUtil {

	@LocalServerPort
	private int port;

	private TestRestTemplate restTemplate = new TestRestTemplate();

	private HttpHeaders headers = new HttpHeaders();

	@Test
	public void testBuscarEstados() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<UfDTO> entity = new HttpEntity<UfDTO>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/estados", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarEstadosComParametros() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<UfDTO> entity = new HttpEntity<UfDTO>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/estados?ordenadoPor=ufNome&direcao=ASC", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarEstadosComParametroOrdenacaoInvalido() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<UfDTO> entity = new HttpEntity<UfDTO>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/estados?ordenadoPor=ufNome1&direcao=ASC", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_BAD_REQUEST.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarEstadosComParametroDirecaoInvalido() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<UfDTO> entity = new HttpEntity<UfDTO>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/estados?ordenadoPor=ufNome&direcao=ASCE", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_BAD_REQUEST.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarEstadosPorStatus() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<UfDTO> entity = new HttpEntity<UfDTO>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/estados", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarEstadoNotFound() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<UfDTO> entity = new HttpEntity<UfDTO>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/estados/RM", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_NOT_FOUND.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarEstado() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<UfDTO> entity = new HttpEntity<UfDTO>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/estados/MG", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarCidades() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<UfDTO> entity = new HttpEntity<UfDTO>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/estados/MG/cidades", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_OK.equals(String.valueOf(response.getStatusCode().value())));
	}
	
	@Test
	public void testBuscarCidadesNotFound() throws ClientProtocolException, IOException, JSONException {
		HttpEntity<UfDTO> entity = new HttpEntity<UfDTO>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/estados/RM/cidades", port), HttpMethod.GET,
				entity, String.class);

		assertTrue(HTTP_STATUS_NOT_FOUND.equals(String.valueOf(response.getStatusCode().value())));
	}

}

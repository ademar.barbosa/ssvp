package br.com.coretecnologia.util;

import org.springframework.http.HttpStatus;

public abstract class TestUtil {

	private 	static final String ENDERECO_BASE_APLICACAO 			= 	"http://localhost:";
	protected 	static final String AUTHORIZATION						= 	"Authorization";
	protected 	static final String LOGIN_ADM 							= 	"ademar.barbosa@gmail.com";
	protected 	static final String SENHA_ADM 							= 	"123";
	protected 	static final String LOGIN_CONSULTA						= 	"consulta@ssvp.com";
	protected 	static final String SENHA_CONSULTA						= 	"123";
	protected 	static final String LOGIN_CONSULTA_UNIDADE				= 	"consulta_unidade@ssvp.com";
	protected 	static final String SENHA_CONSULTA_UNIDADE				= 	"123";
	protected 	static final String HTTP_STATUS_NOT_FOUND 				= 	String.valueOf(HttpStatus.NOT_FOUND.value());
	protected 	static final String HTTP_STATUS_OK 						= 	String.valueOf(HttpStatus.OK.value());
	protected 	static final String HTTP_STATUS_CREATED 				= 	String.valueOf(HttpStatus.CREATED.value());
	protected 	static final String HTTP_STATUS_UNPROCESSABLE_ENTITY 	= 	String.valueOf(HttpStatus.UNPROCESSABLE_ENTITY.value());
	protected 	static final String HTTP_STATUS_NO_CONTENT	 			= 	String.valueOf(HttpStatus.NO_CONTENT.value());
	protected 	static final String HTTP_STATUS_BAD_REQUEST	 			= 	String.valueOf(HttpStatus.BAD_REQUEST.value());
	protected 	static final String HTTP_STATUS_FORBIDDEN	 			= 	String.valueOf(HttpStatus.FORBIDDEN.value());
	
	protected String createURLWithPort(String uri, int port) {
		return ENDERECO_BASE_APLICACAO + port + uri;
	}
	
}

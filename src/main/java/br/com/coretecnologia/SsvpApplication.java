package br.com.coretecnologia;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsvpApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SsvpApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
	}

}

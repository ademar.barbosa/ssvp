package br.com.coretecnologia.comparators;

import java.util.Comparator;

import br.com.coretecnologia.domain.Cidade;

public class CidadeComparator implements Comparator<Cidade> {

	@Override
	public int compare(Cidade o1, Cidade o2) {
		return o1.getCidadeNome().compareTo(o2.getCidadeNome());
	}

}

package br.com.coretecnologia.comparators;

import java.util.Comparator;

import br.com.coretecnologia.domain.Uf;

public class EstadoComparator implements Comparator<Uf> {

	@Override
	public int compare(Uf o1, Uf o2) {
		return o1.getUfSigla().compareTo(o2.getUfSigla());
	}

}

package br.com.coretecnologia.services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.coretecnologia.domain.Cidade;
import br.com.coretecnologia.domain.Endereco;
import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.domain.Reuniao;
import br.com.coretecnologia.domain.Uf;
import br.com.coretecnologia.domain.Unidade;
import br.com.coretecnologia.domain.enums.TipoLogradouro;
import br.com.coretecnologia.domain.enums.TipoUnidade;
import br.com.coretecnologia.dto.UnidadeDTO;
import br.com.coretecnologia.dto.UnidadeEdicaoDTO;
import br.com.coretecnologia.resources.repositores.CidadeRepository;
import br.com.coretecnologia.resources.repositores.EstadoRepository;
import br.com.coretecnologia.resources.repositores.UnidadeRepository;
import br.com.coretecnologia.services.exceptions.DataIntegrityException;
import br.com.coretecnologia.services.exceptions.ObjectNotFoundException;

@Service
public class UnidadeService {

	@Autowired
	private UnidadeRepository unidadeRepository;
	
	@Autowired
	private EstadoRepository estadoRepository;
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Autowired
	private EstadoService estadoService;
	
	@Autowired
	private CidadeService cidadeService;
	
	@Autowired
	private MembroService membroService;
	
	@Autowired
	private SequenceGeneratorService sequenceGeneratorService;
	
	public List<Unidade> findAll() {
		return unidadeRepository.findAll();
	}
	
	public Unidade find(Integer id) {
		Unidade unidade = unidadeRepository.findByUnidadeId(id).orElse(null);

		if (unidade == null) {
			throw new ObjectNotFoundException("Objeto não encontrado! Id: " + id + ", Tipo: " + Unidade.class.getName());
		}
		
		return unidade;
	}
	
	public void save(Unidade unidade) {
		unidadeRepository.save(unidade);
	}

	public void instantiateDevDatabase() {
		unidadeRepository.deleteAll();
		
		Unidade unidade = new Unidade();
		unidade.setUnidadeId(sequenceGeneratorService.generateSequence(Unidade.SEQUENCE_NAME));
		unidade.setUnidadeNome("Conselho Nacional do Brasil");
		unidade.setTelefone("(31) 91234-4657");
		unidade.setEmail("teste@ssvp.com.br");
		unidade.setTipoUnidade(TipoUnidade.CONSELHO_NACIONAL);
		unidade.setUnidadeSuperior(null);
		
		Endereco endereco = new Endereco();
		Cidade cidade = cidadeRepository.findByCidadeId(1).get();
		
		Uf uf = estadoRepository.findByUfSigla("MG").get();
		uf.setCidades(null);
		endereco.setTipoLogradouro(TipoLogradouro.RUA);
		endereco.setLogradouro("Santana");
		endereco.setNumero("1");
		endereco.setComplemento("Casa");
		endereco.setBairro("Água Branca");
		endereco.setCep("32371080");
		endereco.setCidade(cidade);
		endereco.setUf(uf);
		endereco.setReferenciaEndereco("Próximo à rua cardeal de arco verde");
		unidade.setEndereco(endereco);
		
		Reuniao reuniao = new Reuniao();
		reuniao.setDiaSemana("Segunda-feira");
		reuniao.setHorario("20:00");
		unidade.setReuniao(reuniao);
		
		unidadeRepository.save(unidade);
	}

	public Unidade fromDTO(@Valid UnidadeDTO objDTO) {
		Unidade unidade = new Unidade(objDTO);
		return unidade;
	}
	
	public Unidade fromDTO(@Valid UnidadeEdicaoDTO objDTO) {
		Unidade unidade = new Unidade(objDTO);
		return unidade;
	}

	public Unidade insert(Unidade obj) {
		obj.setObjectId(null);
		obj.setUnidadeId(sequenceGeneratorService.generateSequence(Unidade.SEQUENCE_NAME));
		
		if (obj.getUnidadeSuperior() != null && obj.getUnidadeSuperior().getUnidadeId() != null) {
			obj.getUnidadeSuperior().setObjectId(find(obj.getUnidadeSuperior().getUnidadeId()).getObjectId());;
		}
		
		obj.getEndereco().setUf(obterEstadoAtualizado(obj));
		obj.getEndereco().setCidade(obterCidadeAtualizada(obj));
		
		return unidadeRepository.save(obj);
	}

	private Cidade obterCidadeAtualizada(Unidade obj) {
		if (obj.getEndereco().getCidade() != null) {
			Cidade cidade = cidadeService.findByCidadeId(obj.getEndereco().getCidade().getCidadeId());
			return cidade;
		} else {
			return null;
		}
	}

	private Uf obterEstadoAtualizado(Unidade obj) {
		if (obj.getEndereco().getUf() != null) {
			Uf uf = estadoService.find(obj.getEndereco().getUf().getUfSigla());
			uf.setCidades(null);
			return uf;
		} else {
			return null;
		}
	}

	public void delete(Integer unidadeId) {
		Unidade obj = find(unidadeId);
		try {
			List<Unidade> lsUnidadeVinculada = unidadeRepository.findByUnidadeSuperiorUnidadeIdIs(unidadeId);
			
			if (!CollectionUtils.isEmpty(lsUnidadeVinculada)) {
				throw new DataIntegrityException("A unidade em questão está vinculada a uma unidade superior.");
			}
			
			unidadeRepository.delete(obj);
		} catch(DataIntegrityViolationException e) {
			throw new DataIntegrityException("Houve um erro ao excluir a unidade.");
		}
	}

	public Unidade update(Unidade obj) {
		Unidade objAux = find(obj.getUnidadeId());
		obj.setObjectId(objAux.getObjectId());
		
		if (obj.getUnidadeSuperior() != null && obj.getUnidadeSuperior().getUnidadeId() != null) {
			obj.getUnidadeSuperior().setObjectId(find(obj.getUnidadeSuperior().getUnidadeId()).getObjectId());;
		}
		
		obj.getEndereco().setUf(obterEstadoAtualizado(obj));
		obj.getEndereco().setCidade(obterCidadeAtualizada(obj));
		
		return unidadeRepository.save(obj);
	}

	public Page<Unidade> findPaginado(String unidadeNome, String cidadeNome, Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		if (cidadeNome.equals("")) {
			return unidadeRepository.findByUnidadeNomeIgnoreCaseLike(unidadeNome, pageRequest);
		} else {
			return unidadeRepository.findByUnidadeNomeIgnoreCaseLikeAndEnderecoCidadeIs(unidadeNome, cidadeNome, pageRequest);
		}
	}

	public List<Unidade> findUnidades(String tipoUnidade) {
		if (tipoUnidade != null && !tipoUnidade.equals("")) {
			TipoUnidade tpUnidade = TipoUnidade.toEnum(tipoUnidade);
			return unidadeRepository.findByTipoUnidade(tpUnidade);
		} else {
			return findAll();
		}
	}

	public List<Membro> findMembroPorUnidade(Integer unidadeId) {
		Unidade unidade = find(unidadeId);
		List<Membro> lsMembros = membroService.findMembro(unidade.getLsMembros());
		return lsMembros;
	}

	public void associarMembro(Integer unidadeId, Integer membroId) {
		Unidade unidade = find(unidadeId);
		
		unidade.getLsMembros().add(membroId);
		update(unidade);
	}

	public void desassociarMembro(Integer unidadeId, Integer membroId) {
		Unidade unidade = find(unidadeId);
		
		if (unidade == null) {
			throw new DataIntegrityException("A Unidade informada não foi encontrada na nossa base.");
		}

		Membro membro = membroService.findByMembroId(membroId);
		if (membro == null) {
			throw new DataIntegrityException("O Membro informado não foi encontrada na nossa base.");
		}
		
		Boolean existeAssociacao = unidade.getLsMembros().stream().anyMatch(x -> x.intValue() == membroId.intValue());
		if (!existeAssociacao) {
			throw new DataIntegrityException("O Membro informado está associado à unidade.");
		}
		
		unidade.getLsMembros().remove(membroId);
		update(unidade);
	}

	public List<Unidade> findByMembroId(Integer id) {
		return unidadeRepository.findByMembroId(id);
	}
	
}

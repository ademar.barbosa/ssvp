package br.com.coretecnologia.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.coretecnologia.domain.Conferencia;
import br.com.coretecnologia.resources.repositores.ConferenciaRepository;

@Service
public class ConferenciaService {

	@Autowired
	private ConferenciaRepository conferenciaRepository;
	
	public List<Conferencia> findAll() {
		return conferenciaRepository.findAll();
	}

	public Conferencia find(Integer id) {
		return conferenciaRepository.findByConferenciaId(id);
	}
	
}

package br.com.coretecnologia.services.exceptions;

public class InvalidParameterException extends RuntimeException {

	private static final long serialVersionUID = -5710324911861792926L;
	
	public InvalidParameterException(String message) {
		super(message);
	}
	
	public InvalidParameterException(String message, Throwable cause) {
		super(message, cause);
	}

}

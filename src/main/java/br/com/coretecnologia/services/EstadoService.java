package br.com.coretecnologia.services;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.coretecnologia.comparators.CidadeComparator;
import br.com.coretecnologia.comparators.EstadoComparator;
import br.com.coretecnologia.domain.Cidade;
import br.com.coretecnologia.domain.Uf;
import br.com.coretecnologia.resources.repositores.CidadeRepository;
import br.com.coretecnologia.resources.repositores.EstadoRepository;
import br.com.coretecnologia.services.exceptions.InvalidParameterException;
import br.com.coretecnologia.services.exceptions.ObjectNotFoundException;

@Service
public class EstadoService {

	@Autowired
	private EstadoRepository estadoRepository;
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	public List<Uf> findAll() {
		List<Uf> listaUf = estadoRepository.findAll();
		Collections.sort(listaUf, new EstadoComparator());
		return listaUf;
	}
	
	public Uf find(String sigla) {
		Uf uf = estadoRepository.findByUfSigla(sigla).orElse(null);

		if (uf == null) {
			throw new ObjectNotFoundException("Objeto não encontrado! Id: " + sigla + ", Tipo: " + Uf.class.getName());
		}
		
		return uf;
	}

	public List<Cidade> findCidades(String ufSigla) {
		Uf uf = find(ufSigla);
		List<Cidade> listaCidades = uf.getCidades();
		
		Collections.sort(listaCidades, new CidadeComparator());	
				
		return listaCidades;
	}

	public void instantiateDevDatabase() {
		estadoRepository.deleteAll();
		
		List<Cidade> listaCidades = cidadeRepository.findAll();
		Uf uf = new Uf("MG", "Minas Gerais", listaCidades);
		
		estadoRepository.save(uf);
	}

	public List<Uf> findEstados(String orderBy, String direcao) {
		if (!orderBy.equals("")) {
			if (!orderBy.equalsIgnoreCase("ufSigla") && !orderBy.equalsIgnoreCase("ufNome")) {
				throw new InvalidParameterException("Parâmetro inválido! ordenadoPor: " + orderBy + ", direcao: " + direcao);
			}
			
			if (!direcao.equalsIgnoreCase("ASC") && !direcao.equalsIgnoreCase("DESC")) {
				throw new InvalidParameterException("Parâmetro inválido! ordenadoPor: " + orderBy + ", direcao: " + direcao);
			}
			
			Sort ordenacao = new Sort(Sort.Direction.fromString(direcao), orderBy);
			return estadoRepository.findAll(ordenacao);
		} else {
			return estadoRepository.findAll();
		}
	}
	
}

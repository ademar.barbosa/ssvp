package br.com.coretecnologia.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.coretecnologia.domain.Cidade;
import br.com.coretecnologia.resources.repositores.CidadeRepository;
import br.com.coretecnologia.services.exceptions.ObjectNotFoundException;

@Service
public class CidadeService {

	@Autowired
	private CidadeRepository cidadeRepository;
	
	public void instantiateDevDatabase() {
		cidadeRepository.deleteAll();
		
		Cidade cidade1 = new Cidade(1, "Belo Horizonte");
		Cidade cidade2 = new Cidade(2, "Contagem");
		
		cidadeRepository.save(cidade1);
		cidadeRepository.save(cidade2);
	}
	
	public Cidade findByCidadeId(Integer cidadeId) {
		Optional<Cidade> cidade = cidadeRepository.findByCidadeId(cidadeId);
		if (cidade.isPresent()) {
			return cidade.get();
		} else {
			throw new ObjectNotFoundException("Objeto não encontrado! Id: " + cidadeId + ", Tipo: " + Cidade.class.getName());
		}
	}
	
}

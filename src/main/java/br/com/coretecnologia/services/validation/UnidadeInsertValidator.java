package br.com.coretecnologia.services.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.coretecnologia.domain.Unidade;
import br.com.coretecnologia.domain.enums.TipoLogradouro;
import br.com.coretecnologia.domain.enums.TipoUnidade;
import br.com.coretecnologia.dto.UnidadeDTO;
import br.com.coretecnologia.resources.exceptions.FieldMessage;
import br.com.coretecnologia.resources.repositores.UnidadeRepository;

public class UnidadeInsertValidator implements ConstraintValidator<UnidadeInsert, UnidadeDTO> {
	
	@Autowired
	private UnidadeRepository repo;
	
	@Override
	public void initialize(UnidadeInsert ann) {
	}

	@Override
	public boolean isValid(UnidadeDTO objDto, ConstraintValidatorContext context) {
		List<FieldMessage> list = new ArrayList<>();
		
		Unidade unidadeComMesmoIdCadastrado = repo.findByUnidadeId(objDto.getUnidadeId()).orElse(null);
		if (unidadeComMesmoIdCadastrado != null) {
			list.add(new FieldMessage("unidadeId", "{validation.field.entidade.cadastrada.message}"));
		}
		
		Unidade unidadeComMesmoNomeCadastrado = repo.findByUnidadeNome(objDto.getUnidadeNome()).orElse(null);
		if (unidadeComMesmoNomeCadastrado != null) {
			list.add(new FieldMessage("unidadeNome", "{validation.field.entidade.cadastrada.message}"));
		}
		
		if (TipoUnidade.toEnum(objDto.getTipoUnidade()) == null) {
			list.add(new FieldMessage("tipoUnidade", "{validation.field.tipounidade.naoencontrada.message}"));
		}
		
		if (TipoLogradouro.toEnum(objDto.getEndereco().getTipoLogradouro()) == null) {
			list.add(new FieldMessage("tipoLogradouro", "{validation.field.tipologradouro.naoencontrada.message}"));
		}
		
		if (objDto.getUnidadeSuperior() != null && objDto.getUnidadeSuperior().getUnidadeId() != null) {
			Unidade unidadeSuperior = repo.findByUnidadeId(objDto.getUnidadeSuperior().getUnidadeId()).orElse(null);
			if (unidadeSuperior == null) {
				list.add(new FieldMessage("unidadeSuperior", "{validation.field.unidadevinculada.naoencontrada.message}"));
			}
			TipoUnidade tpUnidadeSuperior = TipoUnidade.getTipoUnidadeSuperior(objDto.getTipoUnidade());
			if (unidadeSuperior != null && unidadeSuperior.getTipoUnidade() != tpUnidadeSuperior) {
				list.add(new FieldMessage("unidadeSuperior", "{validation.field.unidadevinculada.erro.hierarquia.message}"));
			}
		}
		
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		
		return list.isEmpty();
	}

}
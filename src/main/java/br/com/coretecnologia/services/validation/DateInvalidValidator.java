package br.com.coretecnologia.services.validation;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateInvalidValidator implements ConstraintValidator<DateInvalid, String> {
	
	private final SimpleDateFormat dateParser = new SimpleDateFormat("ddMMyyyy");
    
    @SuppressWarnings("unused")
	private DateInvalid constraintAnnotation;
	
    @Override
    public void initialize(DateInvalid constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        try {
        	if (value != null && !value.isEmpty()) {
	        	value = value.replaceAll("/", "");
	        	dateParser.parse(value);
        	}
        } catch (ParseException ex) {
        	return false;
        }
        return true;
    }
    
}
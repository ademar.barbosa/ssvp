package br.com.coretecnologia.services.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = DateInvalidValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DateInvalid {
	String message() default "{validation.field.data.invalida.message}"; 

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
	
}
package br.com.coretecnologia.services.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.coretecnologia.domain.Unidade;
import br.com.coretecnologia.domain.enums.TipoLogradouro;
import br.com.coretecnologia.domain.enums.TipoUnidade;
import br.com.coretecnologia.dto.UnidadeEdicaoDTO;
import br.com.coretecnologia.resources.exceptions.FieldMessage;
import br.com.coretecnologia.resources.repositores.UnidadeRepository;
import br.com.coretecnologia.services.exceptions.DataIntegrityException;

public class UnidadeUpdateValidator implements ConstraintValidator<UnidadeUpdate, UnidadeEdicaoDTO> {
	
	@Autowired
	private UnidadeRepository repo;
	
	@Override
	public void initialize(UnidadeUpdate ann) {
	}

	@Override
	public boolean isValid(UnidadeEdicaoDTO objDto, ConstraintValidatorContext context) {
		List<FieldMessage> list = new ArrayList<>();
		
		Unidade unidadeCadastrada = repo.findByUnidadeId(objDto.getUnidadeId()).orElse(null);
		if (unidadeCadastrada == null) {
			throw new DataIntegrityException("A unidade informada não está cadastrada na base.");
		}
		
		Unidade unidadeComMesmoNomeCadastrado = repo.findByUnidadeNome(objDto.getUnidadeNome()).orElse(null);
		if (unidadeComMesmoNomeCadastrado != null && objDto.getUnidadeId().intValue() != unidadeComMesmoNomeCadastrado.getUnidadeId().intValue()) {
			list.add(new FieldMessage("unidadeNome", "{validation.field.entidade.cadastrada.message}"));
		}

		if (TipoUnidade.toEnum(objDto.getTipoUnidade()) == null) {
			list.add(new FieldMessage("tipoUnidade", "{validation.field.tipounidade.naoencontrada.message}"));
		}
		
		if (TipoLogradouro.toEnum(objDto.getEndereco().getTipoLogradouro()) == null) {
			list.add(new FieldMessage("tipoLogradouro", "{validation.field.tipologradouro.naoencontrada.message}"));
		}
		
		if (objDto.getUnidadeSuperior() != null && objDto.getUnidadeSuperior().getUnidadeId() != null) {
			Unidade unidadeSuperior = repo.findByUnidadeId(objDto.getUnidadeSuperior().getUnidadeId()).orElse(null);
			if (unidadeSuperior == null) {
				list.add(new FieldMessage("unidadeSuperior", "{validation.field.unidadevinculada.naoencontrada.message}"));
			}
			TipoUnidade tpUnidadeSuperior = TipoUnidade.getTipoUnidadeSuperior(objDto.getTipoUnidade());
			if (unidadeSuperior != null && unidadeSuperior.getTipoUnidade() != tpUnidadeSuperior) {
				list.add(new FieldMessage("unidadeSuperior", "{validation.field.unidadevinculada.erro.hierarquia.message}"));
			}
		}
		
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		
		return list.isEmpty();
	}
}
package br.com.coretecnologia.services.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = MembroInsertValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface MembroInsert {
	String message() default "{validation.field.notNull.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
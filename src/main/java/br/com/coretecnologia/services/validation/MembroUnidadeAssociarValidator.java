package br.com.coretecnologia.services.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.domain.Unidade;
import br.com.coretecnologia.dto.MembroUnidadeDTO;
import br.com.coretecnologia.resources.exceptions.FieldMessage;
import br.com.coretecnologia.resources.repositores.MembroRepository;
import br.com.coretecnologia.resources.repositores.UnidadeRepository;

public class MembroUnidadeAssociarValidator implements ConstraintValidator<MembroUnidadeAssociar, MembroUnidadeDTO> {
	
	@Autowired
	private UnidadeRepository unidadeRepository;
	
	@Autowired
	private MembroRepository membroRepository;
	
	@Override
	public void initialize(MembroUnidadeAssociar ann) {
	}

	@Override
	public boolean isValid(MembroUnidadeDTO objDto, ConstraintValidatorContext context) {
		List<FieldMessage> list = new ArrayList<>();
		
		Unidade unidade = unidadeRepository.findByUnidadeId(objDto.getUnidadeId()).orElse(null);
		if (unidade == null) {
			list.add(new FieldMessage("unidadeId", "{validation.field.unidade.naoencontrada.message}"));
		}
		
		Membro membro = membroRepository.findByMembroId(objDto.getMembroId());
		if (membro == null) {
			list.add(new FieldMessage("membroId", "{validation.field.membro.naoencontrado.message}"));
		}
		
		Boolean membroJaAssociado = membroRepository.isMembroAssociadoUnidadeByTipoUnidade(unidade.getTipoUnidade(), membro.getMembroId());
		if (membroJaAssociado) {
			list.add(new FieldMessage("membroId", "{validation.field.membro.jaassociado.message}"));
		}
		
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		
		return list.isEmpty();
	}
}
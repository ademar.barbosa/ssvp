package br.com.coretecnologia.services.validation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateRangeStringValidator implements ConstraintValidator<DateRangeString, String> {
	
	private final SimpleDateFormat dateParser = new SimpleDateFormat("ddMMyyyy");
	private final SimpleDateFormat dateParserParam = new SimpleDateFormat("yyyy-MM-dd");
    
    private DateRangeString constraintAnnotation;
	
    @Override
    public void initialize(DateRangeString constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        try {
        	if (value != null && !value.isEmpty()) {
	        	value = value.replaceAll("/", "");
	        	final Date data = dateParser.parse(value);
	        	
	            final Date min = dateParserParam.parse(constraintAnnotation.min());
	            final Date max = dateParserParam.parse(constraintAnnotation.max());
	            return value == null ||
	                    (data.after(min) && data.before(max));
        	}
        } catch (ParseException ex) {
            return false;
        }
		return true;
    }
    
}
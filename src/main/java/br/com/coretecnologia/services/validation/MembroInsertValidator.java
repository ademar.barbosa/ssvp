package br.com.coretecnologia.services.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.domain.enums.SituacaoMembro;
import br.com.coretecnologia.dto.MembroInsertDTO;
import br.com.coretecnologia.resources.exceptions.FieldMessage;
import br.com.coretecnologia.resources.repositores.MembroRepository;

public class MembroInsertValidator implements ConstraintValidator<MembroInsert, MembroInsertDTO> {
	
	@Autowired
	private MembroRepository repo;
	
	@Override
	public void initialize(MembroInsert ann) {
	}

	@Override
	public boolean isValid(MembroInsertDTO objDto, ConstraintValidatorContext context) {
		List<FieldMessage> list = new ArrayList<>();
		
		try {
			SituacaoMembro.toEnum(objDto.getSituacaoMembro());
		} catch(IllegalArgumentException e) {
			list.add(new FieldMessage("situacaoMembro", "{validation.field.situacaomembro.invalido.message}"));
		}
		
		Membro membro = repo.findByEmail(objDto.getEmail());
		if (membro != null) {
			list.add(new FieldMessage("email", "{validation.field.email.existe.message}"));
		}
		
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName()).addConstraintViolation();
		}
		
		return list.isEmpty();
	}

}
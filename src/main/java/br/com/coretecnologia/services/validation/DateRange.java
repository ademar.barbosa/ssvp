package br.com.coretecnologia.services.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = DateRangeValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DateRange {
	String message() default "{validation.date.InDateRange.message}"; 

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
	
    String min() default "1900-01-01";
    String max() default "2999-12-31";
}
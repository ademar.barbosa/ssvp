package br.com.coretecnologia.services.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.domain.enums.SituacaoMembro;
import br.com.coretecnologia.dto.MembroUpdateDTO;
import br.com.coretecnologia.resources.exceptions.FieldMessage;
import br.com.coretecnologia.resources.repositores.MembroRepository;

public class MembroUpdateValidator implements ConstraintValidator<MembroUpdate, MembroUpdateDTO> {

	@Autowired
	private MembroRepository repo;

	@Override
	public void initialize(MembroUpdate ann) {
	}

	@Override
	public boolean isValid(MembroUpdateDTO objDto, ConstraintValidatorContext context) {
		List<FieldMessage> list = new ArrayList<>();
		
		try {
			SituacaoMembro.toEnum(objDto.getSituacaoMembro());
		} catch(IllegalArgumentException e) {
			list.add(new FieldMessage("situacaoMembro", "{validation.field.situacaomembro.invalido.message}"));
		}
		
		Membro membro = repo.findByEmail(objDto.getEmail());
		if (membro != null && objDto.getMembroId() != null && membro.getMembroId().intValue() != objDto.getMembroId().intValue()) {
			list.add(new FieldMessage("email", "{validation.field.email.existe.message}"));
		}
		
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName()).addConstraintViolation();
		}
		
		return list.isEmpty();
	}
}
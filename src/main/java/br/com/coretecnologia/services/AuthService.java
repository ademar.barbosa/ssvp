package br.com.coretecnologia.services;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.resources.repositores.MembroRepository;
import br.com.coretecnologia.services.exceptions.ObjectNotFoundException;

@Service
public class AuthService {

	@Autowired
	private MembroRepository membroRepository;
	
	@Autowired
	private BCryptPasswordEncoder pe;
	
	@Autowired
	private EmailService emailService;
	
	private Random rand = new Random();
	
	
	public void sendNewPassword(String email) {
		Membro membro = membroRepository.findByEmail(email);
	
		if (membro == null) {
			throw new ObjectNotFoundException("Email não encontrado!");
		}
		
		String newPass = newPassword();
		membro.setSenha(pe.encode(newPass));
		
		membroRepository.save(membro);
		
		emailService.sendNewPasswordEmail(membro, newPass);
	}

	private String newPassword() {
		char[] vet = new char[10];
		
		for (int i = 0; i<10; i++) {
			vet[i] = randomChar();
		}
		
		return new String(vet);
	}

	private char randomChar() {
		int opt = rand.nextInt(3);
		if (opt == 0) { // gera um dígito
			return (char) (rand.nextInt(10) + 48);
		} else if (opt == 1) { // gera letra maiúscula
			return (char) (rand.nextInt(26) + 65);
		} else { // gera letra minúscula
			return (char) (rand.nextInt(26) + 97);
		}
	}
	
}

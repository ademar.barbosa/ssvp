package br.com.coretecnologia.services;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.SimpleMailMessage;

import br.com.coretecnologia.domain.Membro;

public interface EmailService {
	
	void sendEmail(SimpleMailMessage msg);
	
	void sendHtmlEmail(MimeMessage msg);

	void sendNewPasswordEmail(Membro membro, String newPass);

}

package br.com.coretecnologia.services;

import java.security.InvalidParameterException;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.domain.Unidade;
import br.com.coretecnologia.domain.enums.Perfil;
import br.com.coretecnologia.domain.enums.SituacaoMembro;
import br.com.coretecnologia.domain.enums.TipoMembro;
import br.com.coretecnologia.domain.enums.TipoUnidade;
import br.com.coretecnologia.dto.MembroDTO;
import br.com.coretecnologia.dto.MembroPesquisaDTO;
import br.com.coretecnologia.resources.repositores.MembroRepository;
import br.com.coretecnologia.services.exceptions.DataIntegrityException;
import br.com.coretecnologia.services.exceptions.ObjectNotFoundException;

@Service
public class MembroService {

	@Autowired
	private MembroRepository membroRepository;
	
	@Autowired
	private UnidadeService unidadeService;
	
	@Autowired
	private SequenceGeneratorService sequenceGeneratorService;
	
	@Autowired
	private BCryptPasswordEncoder pe;

	public void instantiateDevDatabase() {
		membroRepository.deleteAll();
		Membro membro1 = new Membro(null, null, "Ademar Barbosa Coelho Júnior", "ademar.barbosa@gmail.com", pe.encode("123"), "31997324482", new Date(),
				SituacaoMembro.ATIVO, null, TipoMembro.PRESIDENTE);
		membro1.addPerfil(Perfil.ADMIN);
		membro1.setMembroId(sequenceGeneratorService.generateSequence(Membro.SEQUENCE_NAME));
		
		Membro membro2 = new Membro(null, null, "Michelle Aparecida Machado", "michellemachado1@gmail.com", pe.encode("123"), "31998960340", new Date(),
				SituacaoMembro.ATIVO, null, TipoMembro.PRESIDENTE);
		membro2.addPerfil(Perfil.ADMIN);
		membro2.setMembroId(sequenceGeneratorService.generateSequence(Membro.SEQUENCE_NAME));
		
		Membro membro3 = new Membro(null, null, "Perfil Consulta", "consulta@ssvp.com", pe.encode("123"), "31998960340", new Date(),
				SituacaoMembro.ATIVO, null, TipoMembro.TESOUREIRO);
		membro3.addPerfil(Perfil.CONSULTA);
		membro3.setMembroId(sequenceGeneratorService.generateSequence(Membro.SEQUENCE_NAME));
		
		Membro membro4 = new Membro(null, null, "Perfil Consulta Unidade", "consulta_unidade@ssvp.com", pe.encode("123"), "31998960340", new Date(),
				SituacaoMembro.ATIVO, null, TipoMembro.VICE_TESOUREIRO);
		membro4.addPerfil(Perfil.CONSULTA_UNIDADE);
		membro4.setMembroId(sequenceGeneratorService.generateSequence(Membro.SEQUENCE_NAME));
		
		membroRepository.save(membro1);
		membroRepository.save(membro2);
		membroRepository.save(membro3);
		membroRepository.save(membro4);
	}

	public Membro findByEmail(String email) {
		Membro obj = membroRepository.findByEmail(email);
		if (obj == null) {
			throw new ObjectNotFoundException("Objeto não encontrado! E-mail: " + email + ", Tipo: " + Membro.class.getName());
		}
		return obj;
	}
	
	public Membro findByMembroId(Integer membroId) {
		Membro obj = membroRepository.findByMembroId(membroId);
		System.out.println("Service: " + obj);
		if (obj == null) {
			throw new ObjectNotFoundException("Objeto não encontrado! id: " + membroId + ", Tipo: " + Membro.class.getName());
		}
		obj.setSenha("");
		return obj;
	}

	public List<Membro> findAll() {
		return membroRepository.findAll();
	}

	public Membro fromDTO(@Valid MembroDTO objDTO) {
		Membro membro = new Membro(objDTO);
		return membro;
	}

	public Membro inserir(Membro obj) {
		obj.setObjectId(null);
		obj.setMembroId(sequenceGeneratorService.generateSequence(Membro.SEQUENCE_NAME));
		obj.setSenha(pe.encode(obj.getSenha()));
		return membroRepository.save(obj);
	}

	public Membro atualizar(Membro obj) {
		Membro objAux = findByMembroId(obj.getMembroId());
		obj.setObjectId(objAux.getObjectId());
		obj.setSenha(pe.encode(obj.getSenha()));
		
		return membroRepository.save(obj);
	}

	public void delete(Integer id) {
		Membro obj = findByMembroId(id);
		
		try {
			List<Unidade> lsUnidades = unidadeService.findByMembroId(id);
			if (!CollectionUtils.isEmpty(lsUnidades)) {
				throw new DataIntegrityException("Não é possível excluir o membro, pois ele está associado a uma unidade.");
			} else {
				membroRepository.delete(obj);
			}
		} catch(DataIntegrityViolationException e) {
			throw new DataIntegrityException("Houve um erro ao excluir a unidade.");
		}	
	}

	public List<Membro> findMembro(List<Integer> lsMembros) {
		return membroRepository.findByMembroId(lsMembros);
	}

	public List<MembroPesquisaDTO> findByMembroNome(String nome, TipoUnidade tipoUnidade) {
		if (nome == null || nome.isEmpty()) {
			throw new InvalidParameterException("O valor informado para o campo Nome é inválido!");
		}
		
		return membroRepository.findVinculadosByTipoUnidade(tipoUnidade, nome);
	}

}

package br.com.coretecnologia.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.resources.repositores.MembroRepository;
import br.com.coretecnologia.security.UserSS;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private MembroRepository repo;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Membro membro = repo.findByEmail(email);
		if (membro == null) {
			throw new UsernameNotFoundException(email);
		}
		UserSS user = new UserSS(membro.getObjectId(), membro.getEmail(), membro.getSenha(), membro.getPerfis());
		return user;
	}

}

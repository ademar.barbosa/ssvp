package br.com.coretecnologia.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;

import br.com.coretecnologia.domain.Membro;

public abstract class AbstractEmailService implements EmailService {

	@Value("${default.sender}")
	private String sender;
	
	@Override
	public void sendNewPasswordEmail(Membro membro, String newPass) {
		SimpleMailMessage sm = prepareSimpleMailMessageFromMembro(membro, newPass);
		sendEmail(sm);
	}

	protected SimpleMailMessage prepareSimpleMailMessageFromMembro(Membro membro, String newPass) {
		SimpleMailMessage sm = new SimpleMailMessage();
		sm.setTo(membro.getEmail());
		sm.setFrom(sender);
		sm.setSubject("Solicitação de nova senha.");
		sm.setSentDate(new Date(System.currentTimeMillis()));
		sm.setText("Nova senha: " + newPass);
		return sm;
	}
	
}

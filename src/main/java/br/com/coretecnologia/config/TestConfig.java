package br.com.coretecnologia.config;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.domain.Unidade;
import br.com.coretecnologia.services.CidadeService;
import br.com.coretecnologia.services.EmailService;
import br.com.coretecnologia.services.EstadoService;
import br.com.coretecnologia.services.MembroService;
import br.com.coretecnologia.services.SequenceGeneratorService;
import br.com.coretecnologia.services.SmtpEmailService;
import br.com.coretecnologia.services.UnidadeService;

@Configuration
@Profile("test")
public class TestConfig {

	@Autowired
	private UnidadeService unidadeService;
	
	@Autowired
	private EstadoService estadoService;
	
	@Autowired
	private CidadeService cidadeService;
	
	@Autowired
	private MembroService membroService;
	
	@Autowired
	private SequenceGeneratorService sequenceGeneratorService;
	
	@Value("${mongodb.scheme.generation}")
	private String strategy;
	
	@Bean
	public boolean instantiateDatabase() throws ParseException {
		if (!"create".equals(strategy)) {
			return false;
		}
		
		sequenceGeneratorService.iniciarSequence(Membro.SEQUENCE_NAME);
		sequenceGeneratorService.iniciarSequence(Unidade.SEQUENCE_NAME);
		membroService.instantiateDevDatabase();
		cidadeService.instantiateDevDatabase();
		estadoService.instantiateDevDatabase();
		unidadeService.instantiateDevDatabase();
		return true;
	}
	
	@Bean
	public EmailService emailService() {
		return new SmtpEmailService();
	}
	
}

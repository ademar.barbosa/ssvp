package br.com.coretecnologia.resources.repositores;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.coretecnologia.domain.Unidade;
import br.com.coretecnologia.domain.enums.TipoUnidade;

@Repository
public interface UnidadeRepository extends MongoRepository<Unidade, String> {

	public Optional<Unidade> findByUnidadeId(Integer id);

	public Optional<Unidade> findByUnidadeNome(String unidadeNome);

	public List<Unidade> findByUnidadeSuperiorUnidadeIdIs(Integer unidadeId);

	public Page<Unidade> findByUnidadeNomeIgnoreCaseLikeAndEnderecoCidadeIs(String unidadeNome, String cidadeNome, PageRequest pageRequest);
	
	public Page<Unidade> findByUnidadeNomeIgnoreCaseLike(String unidadeNome, PageRequest pageRequest);

	public List<Unidade> findByTipoUnidade(TipoUnidade tpUnidade);
	
	@Query("{lsMembros: ?0 }")
	public List<Unidade> findByMembroId(Integer membroId);

}

package br.com.coretecnologia.resources.repositores;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.coretecnologia.domain.Cidade;

@Repository
public interface CidadeRepository extends MongoRepository<Cidade, String> {

	public Optional<Cidade> findByCidadeId(Integer id);
	
}

package br.com.coretecnologia.resources.repositores;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.coretecnologia.domain.Conferencia;

@Repository
public interface ConferenciaRepository extends MongoRepository<Conferencia, String> {

	Conferencia findByConferenciaId(Integer id);

}

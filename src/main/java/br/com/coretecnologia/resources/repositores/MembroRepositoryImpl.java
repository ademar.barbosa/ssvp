package br.com.coretecnologia.resources.repositores;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.CollectionUtils;

import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.domain.Unidade;
import br.com.coretecnologia.domain.enums.TipoUnidade;
import br.com.coretecnologia.dto.MembroPesquisaDTO;

public class MembroRepositoryImpl implements MembroRepositoryCustom {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	/*public List<Membro> findVinculadosByTipoUnidade(TipoUnidade tipoUnidade) {
		LookupOperation lookupOperation = LookupOperation.newLookup().from("unidades").localField("membroId").foreignField("lsMembros").as("listaMembros");

		Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(Criteria.where("nome").regex("Ade", "i")), lookupOperation);
		
		List<Membro> results = mongoTemplate.aggregate(aggregation, "membros", Membro.class).getMappedResults();
		
		List<MembroDTO> listaMembros =findVinculados();
		
		return results;
	}*/
	
	public List<MembroPesquisaDTO> findVinculadosByTipoUnidade(TipoUnidade tipoUnidade, String nome) {
		List<MembroPesquisaDTO> listaMembros = new ArrayList<>();
		List<Membro> listaMembrosAux = mongoTemplate.find(
				Query.query(Criteria.where("nome").regex(nome, "i")), Membro.class);
		
		for (Membro membro : listaMembrosAux) {
			List<Unidade> listaUnidadesMembroPresente = mongoTemplate.find(
					Query.query(Criteria.where("lsMembros").in(membro.getMembroId())
							.and("tipoUnidade").is(tipoUnidade.name())), 
					Unidade.class); 
			MembroPesquisaDTO membroDTO = new MembroPesquisaDTO(membro);
			if (!CollectionUtils.isEmpty(listaUnidadesMembroPresente)) {
				membroDTO.setEstaAssociado(Boolean.TRUE);
			} else {
				membroDTO.setEstaAssociado(Boolean.FALSE);
			}
			listaMembros.add(membroDTO);
		}
		
		return listaMembros;
	}
	
	public Boolean isMembroAssociadoUnidadeByTipoUnidade(TipoUnidade tipoUnidade, Integer membroId) {
		List<Unidade> listaUnidadesMembroPresente = null;

		listaUnidadesMembroPresente = mongoTemplate.find(
				Query.query(Criteria.where("lsMembros").in(membroId).and("tipoUnidade").is(tipoUnidade.name())), Unidade.class);
		
		if (CollectionUtils.isEmpty(listaUnidadesMembroPresente)) {
			return Boolean.FALSE;
		} else {
			return Boolean.TRUE;
		}
	}
	
}

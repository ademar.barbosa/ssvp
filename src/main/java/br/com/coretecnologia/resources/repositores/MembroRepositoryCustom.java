package br.com.coretecnologia.resources.repositores;

import java.util.List;

import br.com.coretecnologia.domain.enums.TipoUnidade;
import br.com.coretecnologia.dto.MembroPesquisaDTO;

public interface MembroRepositoryCustom {

	public List<MembroPesquisaDTO> findVinculadosByTipoUnidade(TipoUnidade tipoUnidade, String nome);
	
	public Boolean isMembroAssociadoUnidadeByTipoUnidade(TipoUnidade tipoUnidade, Integer membroId);
	
}

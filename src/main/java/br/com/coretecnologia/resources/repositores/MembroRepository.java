package br.com.coretecnologia.resources.repositores;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.coretecnologia.domain.Membro;

@Repository
public interface MembroRepository extends MongoRepository<Membro, String>, MembroRepositoryCustom {

	Membro findByEmail(String email);

	Membro findByMembroId(Integer membroId);

	@Query("{membroId: { $in: ?0 } }")
	List<Membro> findByMembroId(List<Integer> lsMembros);

	@Query(value="{nome: {'$regex' : ?0, '$options' : 'i'} }")
	List<Membro> findByNomeLikeIgnoreCase(String nome);
	
}

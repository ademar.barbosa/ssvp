package br.com.coretecnologia.resources.repositores;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.coretecnologia.domain.Cidade;
import br.com.coretecnologia.domain.Uf;

@Repository
public interface EstadoRepository extends MongoRepository<Uf, String> {

	public Optional<Uf> findByUfSigla(String sigla);

	@Transactional(readOnly=true)
	@Query("{'endereco.uf.ufSigla' : ?0}")
	public List<Cidade> findCidades(String ufSigla);

}

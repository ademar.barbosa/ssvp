package br.com.coretecnologia.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.coretecnologia.domain.Cidade;
import br.com.coretecnologia.domain.Uf;
import br.com.coretecnologia.services.EstadoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Estados", description = "Contém a lógica de controle dos Estados.")
@RequestMapping(value="/estados")
public class EstadoResource {
	
	@Autowired
	private EstadoService service;
	
	@ApiOperation(value = "Retorna a lista de todos os estados cadastrados.")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Uf>> getEstados(
			@RequestParam(value = "ordenadoPor", defaultValue="") String orderBy,
			@RequestParam(value = "direcao", defaultValue="") String direcao
			) {
		List<Uf> lista = service.findEstados(orderBy, direcao);
		return ResponseEntity.ok().body(lista);
	}
	
	@ApiOperation(value = "Retorna o estado solicitado.")
	@RequestMapping(value ="/{ufSigla}", method = RequestMethod.GET)
	public ResponseEntity<Uf> getEstado(@PathVariable String ufSigla) {
		Uf uf = service.find(ufSigla);
		return ResponseEntity.ok().body(uf);
	}
	
	@ApiOperation(value = "Retorna a lista de cidades de um determinado estado.")
	@RequestMapping(value ="/{ufSigla}/cidades", method = RequestMethod.GET)
	public ResponseEntity<List<Cidade>> getCidades(
			@PathVariable String ufSigla
			) {
		List<Cidade> listaCidades = service.findCidades(ufSigla);
		return ResponseEntity.ok().body(listaCidades);
	}

}

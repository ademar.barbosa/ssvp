package br.com.coretecnologia.resources;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.coretecnologia.domain.Conferencia;
import br.com.coretecnologia.dto.ConferenciaDTO;
import br.com.coretecnologia.services.ConferenciaService;

@RestController
@RequestMapping(value="/conferencias")
public class ConferenciaResource {

	@Autowired
	private ConferenciaService service;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ConferenciaDTO>> getConferencias() {
		List<Conferencia> lista = service.findAll();
		List<ConferenciaDTO> listaDTO = lista.stream().map(obj -> new ConferenciaDTO(obj)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listaDTO);
	}
	
	@RequestMapping(value ="/{id}", method = RequestMethod.GET)
	public ResponseEntity<Conferencia> getConferencia(@PathVariable Integer id) {
		Conferencia conferencia = service.find(id);
		return ResponseEntity.ok().body(conferencia);
	}
	
}

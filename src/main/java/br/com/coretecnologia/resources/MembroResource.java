package br.com.coretecnologia.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.domain.enums.TipoUnidade;
import br.com.coretecnologia.dto.MembroDTO;
import br.com.coretecnologia.dto.MembroInsertDTO;
import br.com.coretecnologia.dto.MembroPesquisaDTO;
import br.com.coretecnologia.dto.MembroUpdateDTO;
import br.com.coretecnologia.services.MembroService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Membros", description = "Mantém as informações dos Membros da SSVP")
@RequestMapping(value="/membros")
public class MembroResource {

	@Autowired
	private MembroService service;

	@ApiOperation(value = "Retorna um membro, pelo e-mail informado.")
	@PreAuthorize("hasAnyRole('ADMIN') OR hasAnyRole('CONSULTA')")
	@RequestMapping(value ="/{email:[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$}", method = RequestMethod.GET)
	public ResponseEntity<MembroInsertDTO> find(@PathVariable(value = "email") String email) {
		Membro obj = service.findByEmail(email);
		MembroInsertDTO membroDTO = new MembroInsertDTO(obj);
		return ResponseEntity.ok().body(membroDTO);
	}
	
	@ApiOperation(value = "Retorna um membro, pelo id informado.")
	@PreAuthorize("hasAnyRole('ADMIN') OR hasAnyRole('CONSULTA')")
	@RequestMapping(value ="/{id:[0-9]+}", method = RequestMethod.GET)
	public ResponseEntity<MembroInsertDTO> findById(@PathVariable(value = "id") Integer membroId) {
		System.out.println("### MembroId: " + membroId);
		Membro obj = service.findByMembroId(membroId);
		MembroInsertDTO membroDTO = new MembroInsertDTO(obj);
		return ResponseEntity.ok().body(membroDTO);
	}
	
	@ApiOperation(value = "Retorna a lista de todos os membros cadastrados.")
	@PreAuthorize("hasAnyRole('ADMIN') OR hasAnyRole('CONSULTA')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<MembroDTO>> getMembros() {
		List<Membro> lista = service.findAll();
		List<MembroDTO> listaDTO = lista.stream().map(obj -> new MembroDTO(obj)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listaDTO);
	}
	
	@ApiOperation(value = "Método responsável por inserir um membro.")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<MembroInsertDTO> inserir(@Valid @RequestBody MembroInsertDTO objDTO) {
		Membro obj = service.fromDTO(objDTO);
		obj = service.inserir(obj);
		MembroInsertDTO objAtualizado = new MembroInsertDTO(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(objAtualizado.getMembroId()).toUri();
		return ResponseEntity.created(uri).body(objAtualizado);
	}
	
	@ApiOperation(value = "Método responsável por alterar um membro.")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizar(@Valid @RequestBody MembroUpdateDTO objDTO) {
		Membro obj = service.fromDTO(objDTO);
		obj = service.atualizar(obj);
		return ResponseEntity.noContent().build();
	}
	
	@ApiOperation(value = "Método responsável por excluir um membro.")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value ="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deletar(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	// FIXME: Fazer métodos de testes.
	@ApiOperation(value = "Retorna um membro, pelo nome e tipo de unidade informados.")
	@PreAuthorize("hasAnyRole('ADMIN') OR hasAnyRole('CONSULTA')")
	@RequestMapping(value ="/find/{nome}/{tipoUnidade}", method = RequestMethod.GET)
	public ResponseEntity<List<MembroPesquisaDTO>> findByNome(
			@PathVariable(value = "nome") String nome,
			@PathVariable(value = "tipoUnidade") String tipoUnidade) {
		List<MembroPesquisaDTO> lsObj = service.findByMembroNome(nome, TipoUnidade.toEnum(tipoUnidade));
		return ResponseEntity.ok().body(lsObj);
	}
	
}

package br.com.coretecnologia.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.domain.Unidade;
import br.com.coretecnologia.dto.MembroDTO;
import br.com.coretecnologia.dto.MembroUnidadeDTO;
import br.com.coretecnologia.dto.UnidadeDTO;
import br.com.coretecnologia.dto.UnidadeEdicaoDTO;
import br.com.coretecnologia.services.UnidadeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Unidades", description = "Mantém as Unidades da SSVP")
@RequestMapping(value="/unidades")
public class UnidadeResource {

	@Autowired
	private UnidadeService service;
	
	@ApiOperation(value = "Retorna a lista de todas as unidades cadastradas.")
	@PreAuthorize("hasAnyRole('ADMIN') OR hasAnyRole('CONSULTA') OR hasAnyRole('CONSULTA_UNIDADE')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<UnidadeDTO>> getUnidades(
			@RequestParam(value = "tipoUnidade", defaultValue="") String tipoUnidade) {
		List<Unidade> lista = service.findUnidades(tipoUnidade);
		List<UnidadeDTO> listaDTO = lista.stream().map(obj -> new UnidadeDTO(obj)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listaDTO);
	}
	
	@ApiOperation(value = "Busca uma unidade específica, informando o número da sua matrícula.")
	@PreAuthorize("hasAnyRole('ADMIN') OR hasAnyRole('CONSULTA') OR hasAnyRole('CONSULTA_UNIDADE')")
	@RequestMapping(value ="/{id}", method = RequestMethod.GET)
	public ResponseEntity<UnidadeDTO> getUnidade(@PathVariable Integer id) {
		Unidade unidade = service.find(id);
		UnidadeDTO unidadeDTO = new UnidadeDTO(unidade);
		return ResponseEntity.ok().body(unidadeDTO);
	}
	
	@ApiOperation(value = "Cadastra uma nova unidade")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<UnidadeDTO> inserir(@Valid @RequestBody UnidadeDTO objDTO) {
		Unidade obj = service.fromDTO(objDTO);
		obj = service.insert(obj);
		UnidadeDTO objAtualizado = new UnidadeDTO(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(objAtualizado.getUnidadeId()).toUri();
		return ResponseEntity.created(uri).body(objAtualizado);
	}
	
	@ApiOperation(value = "Atualiza uma unidade")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizar(@Valid @RequestBody UnidadeEdicaoDTO objDTO) {
		Unidade obj = service.fromDTO(objDTO);
		obj = service.update(obj);
		return ResponseEntity.noContent().build();
	}
	
	@ApiOperation(value = "Exclui uma unidade.")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value ="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deletar(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@ApiOperation(value = "Retorna a lista de todas as unidades cadastradas.")
	@PreAuthorize("hasAnyRole('ADMIN') OR hasAnyRole('CONSULTA') OR hasAnyRole('CONSULTA_UNIDADE')")
	@RequestMapping(value = "/paginado", method = RequestMethod.GET)
	public ResponseEntity<Page<UnidadeDTO>> getUnidadesPaginado(
			@RequestParam(value = "unidadeNome", defaultValue="") String unidadeNome,
			@RequestParam(value = "cidadeNome", defaultValue="") String cidadeNome,
			@RequestParam(value = "pagina", defaultValue = "0") Integer page, 
			@RequestParam(value = "linhasPorPagina", defaultValue = "24") Integer linesPerPage, 
			@RequestParam(value = "ordenadoPor", defaultValue = "unidadeNome") String orderBy, 
			@RequestParam(value = "direcao", defaultValue = "ASC") String direction) {
		Page<Unidade> lista = service.findPaginado(unidadeNome, cidadeNome, page, linesPerPage, orderBy, direction);
		Page<UnidadeDTO> listaDTO = lista.map(obj -> new UnidadeDTO(obj));
		return ResponseEntity.ok().body(listaDTO);
	}
	
	@ApiOperation(value = "Efetua a busca dos membros de uma determinada unidade.")
	@PreAuthorize("hasAnyRole('ADMIN') OR hasAnyRole('CONSULTA') OR hasAnyRole('CONSULTA_UNIDADE')")
	@RequestMapping(value ="/{unidadeId}/membros", method = RequestMethod.GET)
	public ResponseEntity<List<MembroDTO>> getMembrosUnidade(@PathVariable Integer unidadeId) {
		List<Membro> lsMembros = service.findMembroPorUnidade(unidadeId);
		List<MembroDTO> lsMembroDTO = lsMembros.stream().map((Membro m) -> new MembroDTO(m)).collect(Collectors.toList());
		return ResponseEntity.ok().body(lsMembroDTO);
	}

	@ApiOperation(value = "Associa um membro a unidade.")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value ="/associarMembro", method = RequestMethod.PUT)
	public ResponseEntity<Void> associarMembro(@Valid @RequestBody MembroUnidadeDTO objDTO) {
		service.associarMembro(objDTO.getUnidadeId(), objDTO.getMembroId());
		return ResponseEntity.noContent().build();
	}
	
	@ApiOperation(value = "Associa um membro a unidade.")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value ="/desassociarMembro/{unidadeId}/{membroId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> desassociarMembro(
			@PathVariable(value = "unidadeId") Integer unidadeId,
			@PathVariable(value = "membroId") Integer membroId) {
		service.desassociarMembro(unidadeId, membroId);
		return ResponseEntity.noContent().build();
	}
	
}

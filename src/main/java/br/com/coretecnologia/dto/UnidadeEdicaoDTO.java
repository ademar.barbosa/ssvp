package br.com.coretecnologia.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import br.com.coretecnologia.domain.Unidade;
import br.com.coretecnologia.services.validation.UnidadeUpdate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@UnidadeUpdate
public class UnidadeEdicaoDTO {

	@NotNull(message = "{validation.field.notNull.message}")
	private Integer unidadeId;
	
	@NotNull(message = "{validation.field.notNull.message}")
	private String unidadeNome;
	
	private String email;
	
	private String telefone;
	
	@Valid
	private EnderecoDTO endereco;
	
	@Valid
	private ReuniaoDTO reuniao;
	
	@NotNull(message = "{validation.field.notNull.message}")
	private String tipoUnidade;
	
	private UnidadeSuperiorDTO unidadeSuperior;
	
	public UnidadeEdicaoDTO(Unidade unidade) {
		this.unidadeId = unidade.getUnidadeId();
		this.unidadeNome = unidade.getUnidadeNome();
		this.email = unidade.getEmail();
		this.telefone = unidade.getTelefone();
		this.endereco = new EnderecoDTO(unidade.getEndereco());
		this.reuniao = new ReuniaoDTO(unidade.getReuniao());
		this.tipoUnidade = unidade.getTipoUnidade().getDescricao();
		this.unidadeSuperior = new UnidadeSuperiorDTO(unidade.getUnidadeSuperior());
	}
	
	public UnidadeEdicaoDTO(UnidadeDTO unidadeDTO) {
		this.unidadeId = unidadeDTO.getUnidadeId();
		this.unidadeNome = unidadeDTO.getUnidadeNome();
		this.email = unidadeDTO.getEmail();
		this.telefone = unidadeDTO.getTelefone();
		this.endereco = unidadeDTO.getEndereco();
		this.reuniao = unidadeDTO.getReuniao();
		this.tipoUnidade = unidadeDTO.getTipoUnidade();
		this.unidadeSuperior = unidadeDTO.getUnidadeSuperior();
	}
	
}

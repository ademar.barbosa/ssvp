package br.com.coretecnologia.dto;

import javax.validation.constraints.NotNull;

import br.com.coretecnologia.domain.Reuniao;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReuniaoDTO {

	@NotNull(message = "{validation.field.notNull.message}")
	private String diaSemana;

	@NotNull(message = "{validation.field.notNull.message}")
	private String horario;
	
	public ReuniaoDTO(Reuniao reuniao) {
		this.diaSemana = reuniao.getDiaSemana();
		this.horario = reuniao.getHorario();
	}

}

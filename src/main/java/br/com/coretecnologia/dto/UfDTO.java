package br.com.coretecnologia.dto;

import br.com.coretecnologia.domain.Uf;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UfDTO {
	
	private String ufSigla;
	
	private String ufNome;
	
	public UfDTO(Uf uf) {
		this.ufSigla = uf.getUfSigla();
		this.ufNome = uf.getUfNome();
	}

}

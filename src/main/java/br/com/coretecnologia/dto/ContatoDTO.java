package br.com.coretecnologia.dto;

import javax.validation.constraints.NotNull;

import br.com.coretecnologia.domain.Contato;
import br.com.coretecnologia.domain.enums.TipoMembro;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContatoDTO {

	@NotNull(message = "{validation.field.notNull.message}")
	private String nome;

	@NotNull(message = "{validation.field.notNull.message}")
	private String telefone;

	@NotNull(message = "{validation.field.notNull.message}")
	private String celular;

	@NotNull(message = "{validation.field.notNull.message}")
	private TipoMembro tipoContato;
	
	public ContatoDTO(Contato obj) {
		this.nome = obj.getNome();
		this.telefone = obj.getTelefone();
		this.celular = obj.getCelular();
		this.tipoContato = obj.getTipoContato();
	}

}

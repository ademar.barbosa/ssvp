package br.com.coretecnologia.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import org.springframework.data.annotation.Transient;

import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.services.validation.MembroInsert;

@MembroInsert
public class MembroInsertDTO extends MembroDTO implements Serializable {

	private static final long serialVersionUID = -8164970613514814075L;
	
	@Transient
	protected static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	public MembroInsertDTO() {
		
	}
	
	public MembroInsertDTO(Membro membro) {
		this.setMembroId(membro.getMembroId());
		this.setNome(membro.getNome());
		this.setEmail(membro.getEmail());
		this.setTelefone(membro.getTelefone());
		this.setSenha(membro.getSenha());
		if (membro.getDataNascimento() != null) {
			this.setDataNascimento(sdf.format(membro.getDataNascimento()));
		}
		if (membro.getTipoMembro() != null) {
			this.setTipoMembro(membro.getTipoMembro().getDescricao());
		}
		this.setPerfil(membro.getPerfis().get(0).getDescricaoGeral());
		this.setSituacaoMembro(membro.getSituacaoMembro().getDescricao());
	}
	
}

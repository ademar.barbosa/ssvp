package br.com.coretecnologia.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class MembroUnidadeDTO {

	@NotNull(message = "{validation.field.notNull.message}")
	private Integer unidadeId;
	
	@NotNull(message = "{validation.field.notNull.message}")
	private Integer membroId;
	
}

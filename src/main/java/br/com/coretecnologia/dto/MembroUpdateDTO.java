package br.com.coretecnologia.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import org.springframework.data.annotation.Transient;

import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.services.validation.MembroUpdate;

@MembroUpdate
public class MembroUpdateDTO extends MembroDTO implements Serializable {

	private static final long serialVersionUID = -8164970613514814075L;
	
	@Transient
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	public MembroUpdateDTO() {
		
	}
	
	public MembroUpdateDTO(Membro membro) {
		this.setMembroId(membro.getMembroId());
		this.setNome(membro.getNome());
		this.setEmail(membro.getEmail());
		this.setTelefone(membro.getTelefone());
		if (membro.getDataNascimento() != null) {
			this.setDataNascimento(sdf.format(membro.getDataNascimento()));
		}
		this.setSituacaoMembro(membro.getSituacaoMembro().getDescricao());
	}
	
}

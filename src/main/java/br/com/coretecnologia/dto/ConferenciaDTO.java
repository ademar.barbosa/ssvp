package br.com.coretecnologia.dto;

import br.com.coretecnologia.domain.Conferencia;

public class ConferenciaDTO {

	private Integer conferenciaId;

	private String nomeConferencia;

	public ConferenciaDTO(Conferencia obj) {
		this.conferenciaId = obj.getConferenciaId();
		this.nomeConferencia = obj.getNomeConferencia();
	}

	public Integer getConferenciaId() {
		return conferenciaId;
	}

	public void setConferenciaId(Integer conferenciaId) {
		this.conferenciaId = conferenciaId;
	}

	public String getNomeConferencia() {
		return nomeConferencia;
	}

	public void setNomeConferencia(String nomeConferencia) {
		this.nomeConferencia = nomeConferencia;
	}

}

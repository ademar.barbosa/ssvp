package br.com.coretecnologia.dto;

import javax.validation.constraints.NotNull;

import br.com.coretecnologia.domain.Cidade;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CidadeDTO {

	@NotNull(message = "{validation.field.notNull.message}")
	private Integer cidadeId;
	
	private String cidadeNome;
	
	public CidadeDTO(Cidade cidade) {
		this.cidadeId = cidade.getCidadeId();
		this.cidadeNome = cidade.getCidadeNome();
	}
	
}

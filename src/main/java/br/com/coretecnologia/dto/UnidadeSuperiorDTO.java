package br.com.coretecnologia.dto;

import br.com.coretecnologia.domain.UnidadeSuperior;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnidadeSuperiorDTO {

	private String objectId;
	private Integer unidadeId;
	private String unidadeNome;

	public UnidadeSuperiorDTO(UnidadeSuperior unidadeSuperior) {
		this.objectId = unidadeSuperior.getObjectId();
		this.unidadeId = unidadeSuperior.getUnidadeId();
		this.unidadeNome = unidadeSuperior.getUnidadeNome();
	}
	
}

package br.com.coretecnologia.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Transient;

import br.com.coretecnologia.domain.Membro;
import br.com.coretecnologia.services.validation.DateInvalid;
import br.com.coretecnologia.services.validation.DateRangeString;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MembroDTO implements Serializable {

	private static final long serialVersionUID = -5934329165529990333L;
	
	@Transient
	protected static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	private Integer membroId;
	
	@NotNull(message = "{validation.field.notNull.message}")
	@NotEmpty(message = "{validation.field.notNull.message}")
	private String nome;
	
	@NotNull(message = "{validation.field.notNull.message}")
	@NotEmpty(message = "{validation.field.notNull.message}")
	@Email(message = "{validation.field.email.invalido.message}")
	private String email;
	
	@NotNull(message = "{validation.field.notNull.message}")
	@NotEmpty(message = "{validation.field.notNull.message}")
	@Size(min=10, max=11, message="{validation.field.telefone.invalido.message}")
	private String telefone;
	
	@NotEmpty(message = "{validation.field.notNull.message}")
	@NotNull(message = "{validation.field.notNull.message}")
	@DateInvalid
	@DateRangeString
	private String dataNascimento;
	
	@NotEmpty(message = "{validation.field.notNull.message}")
	@NotNull(message = "{validation.field.notNull.message}")
	private String situacaoMembro;
	
	@NotEmpty(message = "{validation.field.notNull.message}")
	@NotNull(message = "{validation.field.notNull.message}")
	private String tipoMembro;
	
	@NotEmpty(message = "{validation.field.notNull.message}")
	@NotNull(message = "{validation.field.notNull.message}")
	private String perfil;
	
	private String senha;
	
	public MembroDTO(Membro membro) {
		this.setMembroId(membro.getMembroId());
		this.setNome(membro.getNome());
		this.setEmail(membro.getEmail());
		this.setTelefone(membro.getTelefone());
		if (membro.getDataNascimento() != null) {
			this.setDataNascimento(sdf.format(membro.getDataNascimento()));
		}
		this.setSituacaoMembro(membro.getSituacaoMembro().getDescricao());
		if (membro.getTipoMembro() != null) {
			this.setTipoMembro(membro.getTipoMembro().getDescricao());
		}
		if (membro.getPerfis() != null && !membro.getPerfis().isEmpty()) {
			this.setPerfil(membro.getPerfis().get(0).getDescricao());
		}
	}
	
}

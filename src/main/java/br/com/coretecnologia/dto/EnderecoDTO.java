package br.com.coretecnologia.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import br.com.coretecnologia.domain.Endereco;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EnderecoDTO {
	
	@NotNull(message = "{validation.field.notNull.message}")
	private String tipoLogradouro;

	@NotNull(message = "{validation.field.notNull.message}")
	private String logradouro;

	@NotNull(message = "{validation.field.notNull.message}")
	private String numero;

	@NotNull(message = "{validation.field.notNull.message}")
	private String complemento;

	@NotNull(message = "{validation.field.notNull.message}")
	private String bairro;

	@NotNull(message = "{validation.field.notNull.message}")
	private String cep;

	@Valid
	private CidadeDTO cidade;

	@NotNull(message = "{validation.field.notNull.message}")
	private UfDTO uf;

	private String referenciaEndereco;
	
	public EnderecoDTO(Endereco endereco) {
		this.tipoLogradouro = endereco.getTipoLogradouro().getDescricao();
		this.logradouro = endereco.getLogradouro();
		this.numero = endereco.getNumero();
		this.complemento = endereco.getComplemento();
		this.bairro = endereco.getBairro();
		this.cep = endereco.getCep();
		this.cidade = new CidadeDTO(endereco.getCidade());
		this.uf = new UfDTO(endereco.getUf());
		this.referenciaEndereco = endereco.getReferenciaEndereco();
	}

}

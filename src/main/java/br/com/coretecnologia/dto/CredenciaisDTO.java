package br.com.coretecnologia.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CredenciaisDTO implements Serializable {

	private static final long serialVersionUID = -5751374915658860715L;

	private String email;
	private String senha;
	
}

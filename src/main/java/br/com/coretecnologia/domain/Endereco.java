package br.com.coretecnologia.domain;

import br.com.coretecnologia.domain.enums.TipoLogradouro;
import br.com.coretecnologia.dto.EnderecoDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Endereco {

	private TipoLogradouro tipoLogradouro;

	private String logradouro;

	private String numero;

	private String complemento;

	private String bairro;

	private String cep;

	private Cidade cidade;

	private Uf uf;

	private String referenciaEndereco;
	
	public Endereco(EnderecoDTO endereco) {
		if (endereco != null) {
			this.tipoLogradouro = TipoLogradouro.toEnum(endereco.getTipoLogradouro());
			this.logradouro = endereco.getLogradouro();
			this.numero = endereco.getNumero();
			this.complemento = endereco.getComplemento();
			this.bairro = endereco.getBairro();
			this.cep = endereco.getCep();
			this.cidade = new Cidade(endereco.getCidade());
			this.uf = new Uf(endereco.getUf());
			this.referenciaEndereco = endereco.getReferenciaEndereco();
		}
	}

}

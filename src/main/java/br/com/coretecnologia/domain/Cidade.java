package br.com.coretecnologia.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import br.com.coretecnologia.dto.CidadeDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "cidades")
public class Cidade {

	@Id
	private String id;
	
	private Integer cidadeId;
	
	private String cidadeNome;
	
	public Cidade(CidadeDTO cidadeDTO) {
		this.cidadeId = cidadeDTO.getCidadeId();
		this.cidadeNome = cidadeDTO.getCidadeNome();
	}
	
	public Cidade(Integer cidadeId, String cidadeNome) {
		this.cidadeId = cidadeId;
		this.cidadeNome = cidadeNome;
	}
	
}

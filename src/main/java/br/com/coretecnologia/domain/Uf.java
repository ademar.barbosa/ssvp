package br.com.coretecnologia.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import br.com.coretecnologia.dto.UfDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "estados")
public class Uf {

	@Id
	private String id;
	
	private String ufSigla;
	
	private String ufNome;
	
	@Builder.Default
	private List<Cidade> cidades = new ArrayList<>();
	
	public Uf(UfDTO ufDTO) {
		this.ufSigla = ufDTO.getUfSigla();
		this.ufNome = ufDTO.getUfNome();
	}
	
	public Uf(String ufSigla, String ufNome, List<Cidade> listaCidades) {
		this.ufSigla = ufSigla;
		this.ufNome = ufNome;
		this.cidades = listaCidades;
	}
	
}

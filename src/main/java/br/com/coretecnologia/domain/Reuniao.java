package br.com.coretecnologia.domain;

import br.com.coretecnologia.dto.ReuniaoDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Reuniao {
	
	private String diaSemana;
	
	private String horario;

	public Reuniao(ReuniaoDTO reuniao) {
		if (reuniao != null) {
			this.diaSemana = reuniao.getDiaSemana();
			this.horario = reuniao.getHorario();
		}
	}
	
}

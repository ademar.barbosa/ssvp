package br.com.coretecnologia.domain;

import br.com.coretecnologia.domain.enums.TipoMembro;
import br.com.coretecnologia.dto.ContatoDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Contato {

	private String nome;
	
	private String telefone;
	
	private String celular;
	
	private TipoMembro tipoContato;
	
	public Contato(ContatoDTO obj) {
		this.nome = obj.getNome();
		this.telefone = obj.getTelefone();
		this.celular = obj.getCelular();
		this.tipoContato = obj.getTipoContato();
	}
	
}

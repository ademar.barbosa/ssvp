package br.com.coretecnologia.domain;

import org.springframework.data.annotation.Id;

public class Conferencia {

	@Id
	private String id;
	
	private Integer conferenciaId;
	
	private String nomeConferencia;

	public Integer getConferenciaId() {
		return conferenciaId;
	}

	public void setConferenciaId(Integer conferenciaId) {
		this.conferenciaId = conferenciaId;
	}

	public String getNomeConferencia() {
		return nomeConferencia;
	}

	public void setNomeConferencia(String nomeConferencia) {
		this.nomeConferencia = nomeConferencia;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}

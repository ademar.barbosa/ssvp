package br.com.coretecnologia.domain.enums;

public enum Perfil {

	ADMIN(1, "ROLE_ADMIN", "Administrador"),
	CONSULTA(2, "ROLE_CONSULTA", "Consulta"),
	CONSULTA_UNIDADE(3, "ROLE_CONSULTA_UNIDADE", "Consulta Unidade");
	
	private int codigo;
	private String descricao;
	private String descricaoGeral;
	
	private Perfil(int codigo, String descricao, String descricaoGeral) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.descricaoGeral = descricaoGeral;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public String getDescricaoGeral() {
		return descricaoGeral;
	}

	public void setDescricaoGeral(String descricaoGeral) {
		this.descricaoGeral = descricaoGeral;
	}

	public static Perfil toEnum(Integer codigo) {
		if (codigo == null) {
			return null;
		}
		
		for(Perfil x : Perfil.values()) {
			if (codigo.equals(x.getCodigo())) {
				return x;
			}
		}
		
		throw new IllegalArgumentException("Id inválido: " + codigo);
	}
	
	public static Perfil toEnum(String descricao) {
		if (descricao == null) {
			return null;
		}
		
		if (descricao.equals("Administrador")) {
			return Perfil.ADMIN;
		} else if (descricao.equals("Consulta")) {
			return Perfil.CONSULTA;
		} else if (descricao.equals("Consulta Unidade")) {
			return Perfil.CONSULTA_UNIDADE;
		} else {
			throw new IllegalArgumentException("Descrição inválida: " + descricao);
		}
	}
	
}

package br.com.coretecnologia.domain.enums;

import br.com.coretecnologia.services.exceptions.InvalidParameterException;
import lombok.Getter;

@Getter
public enum TipoUnidade {

	CONFERENCIA(1, "Conferência"),
	CONSELHO_PARTICULAR(2, "Conselho Particular"),
	CONSELHO_CENTRAL(3, "Conselho Central"), 
	CONSELHO_METROPOLITANO(4, "Conselho Metropolitano"), 
	REGIAO(5, "Região"), 
	CONSELHO_NACIONAL(6, "Conselho Nacional");
	
	private int codigo;
	private String descricao;
	
	private TipoUnidade(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public static TipoUnidade toEnum(Integer codigo) {
		if (codigo == null) {
			return null;
		}
		
		for(TipoUnidade x : TipoUnidade.values()) {
			if (codigo.equals(x.getCodigo())) {
				return x;
			}
		}
		
		throw new IllegalArgumentException("Id inválido: " + codigo);
	}
	
	public static TipoUnidade toEnum(String descricao) {
		if (descricao == null) {
			return null;
		}
		
		for(TipoUnidade x : TipoUnidade.values()) {
			if (descricao.equals(x.getDescricao())) {
				return x;
			}
		}
		
		throw new InvalidParameterException("Descrição inválida: " + descricao);
	}
	
	public static TipoUnidade getTipoUnidadeSuperior(String tipoUnidade) {
		TipoUnidade tpUnidade = TipoUnidade.toEnum(tipoUnidade);
		switch(tpUnidade) {
	      case CONFERENCIA: {
	         return TipoUnidade.CONSELHO_PARTICULAR;
	      }
	      case CONSELHO_PARTICULAR: {
	         return TipoUnidade.CONSELHO_CENTRAL;
	      }
	      case CONSELHO_CENTRAL: {
	        return TipoUnidade.CONSELHO_METROPOLITANO;
	      }
	      case CONSELHO_METROPOLITANO: {
	        return TipoUnidade.REGIAO;
	      }
	      case REGIAO: {
	        return TipoUnidade.CONSELHO_NACIONAL;
	      }
	      default: {
	        return null;
	      }
	   }
	}
	
}


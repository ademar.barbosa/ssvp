package br.com.coretecnologia.domain.enums;

import lombok.Getter;

@Getter
public enum TipoLogradouro {

	RUA(1, "Rua"),
	Avenida(2, "Avenida"),
	PRACA(3, "Praça");
	
	private int codigo;
	private String descricao;
	
	private TipoLogradouro(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public static TipoLogradouro toEnum(Integer codigo) {
		if (codigo == null) {
			return null;
		}
		
		for(TipoLogradouro x : TipoLogradouro.values()) {
			if (codigo.equals(x.getCodigo())) {
				return x;
			}
		}
		
		throw new IllegalArgumentException("Id inválido: " + codigo);
	}
	
	public static TipoLogradouro toEnum(String descricao) {
		if (descricao == null) {
			return null;
		}
		
		for(TipoLogradouro x : TipoLogradouro.values()) {
			if (descricao.equals(x.getDescricao())) {
				return x;
			}
		}
		
		throw new IllegalArgumentException("Descrição inválida: " + descricao);
	}
	
}


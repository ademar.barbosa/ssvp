package br.com.coretecnologia.domain.enums;

import lombok.Getter;

@Getter
public enum TipoMembro {

	PRESIDENTE(1, "Presidente"),
	VICE_PRESIDENTE(2, "Vice Presidente"),
	SECRETARIO(3, "Secretário(a)"),
	VICE_SECRETARIO(3, "Vice Secretário(a)"),
	TESOUREIRO(3, "Tesoureiro(a)"),
	VICE_TESOUREIRO(3, "Vice Tesoureiro(a)");
	
	private int codigo;
	private String descricao;
	
	private TipoMembro(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public static TipoMembro toEnum(Integer codigo) {
		if (codigo == null) {
			return null;
		}
		
		for(TipoMembro x : TipoMembro.values()) {
			if (codigo.equals(x.getCodigo())) {
				return x;
			}
		}
		
		throw new IllegalArgumentException("Id inválido: " + codigo);
	}
	
	public static TipoMembro toEnum(String descricao) {
		if (descricao == null) {
			return null;
		}
		
		for(TipoMembro x : TipoMembro.values()) {
			if (descricao.equals(x.getDescricao())) {
				return x;
			}
		}
		
		throw new IllegalArgumentException("Descrição inválida: " + descricao);
	}
	
}


package br.com.coretecnologia.domain.enums;

import lombok.Getter;

@Getter
public enum SituacaoMembro {

	ATIVO(1, "Ativo"),
	AFASTADO(2, "Afastado"),
	FALECIDO(3, "Falecido");
	
	private int codigo;
	private String descricao;
	
	private SituacaoMembro(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public static SituacaoMembro toEnum(Integer codigo) {
		if (codigo == null) {
			return null;
		}
		
		for(SituacaoMembro x : SituacaoMembro.values()) {
			if (codigo.equals(x.getCodigo())) {
				return x;
			}
		}
		
		throw new IllegalArgumentException("Id inválido: " + codigo);
	}
	
	public static SituacaoMembro toEnum(String descricao) {
		if (descricao == null) {
			return null;
		}
		
		for(SituacaoMembro x : SituacaoMembro.values()) {
			if (descricao.equals(x.getDescricao())) {
				return x;
			}
		}
		
		throw new IllegalArgumentException("Descrição inválida: " + descricao);
	}
	
}


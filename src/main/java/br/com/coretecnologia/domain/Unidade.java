package br.com.coretecnologia.domain;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import br.com.coretecnologia.domain.enums.TipoUnidade;
import br.com.coretecnologia.dto.UnidadeDTO;
import br.com.coretecnologia.dto.UnidadeEdicaoDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "unidades")
public class Unidade {

	@Transient
    public static final String SEQUENCE_NAME = "users_sequence";
	
	@Id
	private String objectId;
	
	@Indexed
	@ApiModelProperty(notes = "Número único, representando a matrícula da unidade da SSVP.")
	private Integer unidadeId;
	
	@Indexed
	@ApiModelProperty(notes = "Nome (campo único) da unidade da SSVP.")
	private String unidadeNome;
	
	private String email;
	
	private String telefone;
	
	private Endereco endereco;
	
	private Reuniao reuniao;
	
	private TipoUnidade tipoUnidade;
	
	private UnidadeSuperior unidadeSuperior;
	
	@Builder.Default
	private List<Integer> lsMembros = new ArrayList<>();
	
	public Unidade(UnidadeDTO unidadeDTO) {
		this.unidadeId = unidadeDTO.getUnidadeId();
		this.unidadeNome = unidadeDTO.getUnidadeNome();
		this.email = unidadeDTO.getEmail();
		this.telefone = unidadeDTO.getTelefone();
		this.endereco = new Endereco(unidadeDTO.getEndereco());
		this.reuniao = new Reuniao(unidadeDTO.getReuniao());
		this.tipoUnidade = TipoUnidade.toEnum(unidadeDTO.getTipoUnidade());
		if (unidadeDTO.getUnidadeSuperior() != null) {
			this.unidadeSuperior = new UnidadeSuperior(unidadeDTO.getUnidadeSuperior());
		}
	}

	public Unidade(@Valid UnidadeEdicaoDTO unidadeEdicaoDTO) {
		this.unidadeId = unidadeEdicaoDTO.getUnidadeId();
		this.unidadeNome = unidadeEdicaoDTO.getUnidadeNome();
		this.email = unidadeEdicaoDTO.getEmail();
		this.telefone = unidadeEdicaoDTO.getTelefone();
		this.endereco = new Endereco(unidadeEdicaoDTO.getEndereco());
		this.reuniao = new Reuniao(unidadeEdicaoDTO.getReuniao());
		this.tipoUnidade = TipoUnidade.toEnum(unidadeEdicaoDTO.getTipoUnidade());
		this.unidadeSuperior = new UnidadeSuperior(unidadeEdicaoDTO.getUnidadeSuperior());
	}
	
}

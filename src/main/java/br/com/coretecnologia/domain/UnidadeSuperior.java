package br.com.coretecnologia.domain;

import br.com.coretecnologia.dto.UnidadeSuperiorDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnidadeSuperior {

	private String objectId;
	private Integer unidadeId;
	private String unidadeNome;
	
	public UnidadeSuperior(UnidadeSuperiorDTO unidadeSuperiorDTO) {
		if (unidadeSuperiorDTO != null) {
			this.objectId = unidadeSuperiorDTO.getObjectId();
			this.unidadeId = unidadeSuperiorDTO.getUnidadeId();
			this.unidadeNome = unidadeSuperiorDTO.getUnidadeNome();
		}
	}
	
}

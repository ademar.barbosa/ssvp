package br.com.coretecnologia.domain;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.CollectionUtils;

import br.com.coretecnologia.domain.enums.Perfil;
import br.com.coretecnologia.domain.enums.SituacaoMembro;
import br.com.coretecnologia.domain.enums.TipoMembro;
import br.com.coretecnologia.dto.MembroDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "membros")
public class Membro implements Serializable {

	private static final long serialVersionUID = 1662039102856724314L;
	
	@Transient
	@Builder.Default
	private static final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
	
	@Transient
    public static final String SEQUENCE_NAME = "membros_sequence";

	@Id
	private String objectId;
	private Integer membroId;
	
	@TextIndexed
	private String nome;
	private String email;
	private String senha;
	private String telefone;
	private Date dataNascimento;
	private SituacaoMembro situacaoMembro;
	
	@Builder.Default
	private List<Integer> perfis = new ArrayList<>();
	
	private TipoMembro tipoMembro;
	
	public List<Perfil> getPerfis() {
		if (perfis == null) {
			return null;
		}
		return perfis.stream().map(x -> Perfil.toEnum(x)).collect(Collectors.toList());
	}
	
	public void addPerfil(Perfil perfil) {
		if (CollectionUtils.isEmpty(perfis)) {
			perfis = new ArrayList<>();
		}
		perfis.add(perfil.getCodigo());
	}

	public Membro(@Valid MembroDTO objDTO) {
		this.membroId = objDTO.getMembroId();
		this.nome = objDTO.getNome();
		this.email = objDTO.getEmail();
		this.telefone = objDTO.getTelefone();
		this.senha = objDTO.getSenha();
		try {
			objDTO.setDataNascimento(objDTO.getDataNascimento().replaceAll("/", ""));
			this.dataNascimento = sdf.parse(objDTO.getDataNascimento());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.addPerfil(Perfil.toEnum(objDTO.getPerfil()));
		this.tipoMembro = TipoMembro.toEnum(objDTO.getTipoMembro());
		this.situacaoMembro = SituacaoMembro.toEnum(objDTO.getSituacaoMembro());
	}
	
}

build:
	docker build -t ssvp/springboot .

start_it:
	docker run -it --v $(PWD):/app -p 8080:8080 --name ssvp_backend --rm sspv/springboot

start_d:
	docker run -d $(PWD):/app -p 8080:8080 --name ssvp_backend --rm sspv/springboot

stop:
	docker container stop ssvp_backend
	
compose_recreate:
	docker-compose up --build --force-recreate
	
compose:
	docker-compose up